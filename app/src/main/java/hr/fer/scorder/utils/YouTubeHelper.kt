import java.util.regex.Pattern

object YouTubeHelper {
    val youTubeUrlRegEx = "^(http(s)?:\\/\\/)?((w){3}.)?youtu(be|.be)?(\\.com)?\\/.+"
    val videoIdRegex = "^https?\\:\\/\\/(?:www\\.youtube(?:\\-nocookie)?\\.com\\/|m\\.youtube\\.com\\/|youtube\\.com\\/)?(?:ytscreeningroom\\?vi?=|youtu\\.be\\/|vi?\\/|user\\/.+\\/u\\/\\w{1,2}\\/|embed\\/|watch\\?(?:.*\\&)?vi?=|\\&vi?=|\\?(?:.*\\&)?vi?=)([^#\\&\\?\\n\\/<>\"']*)".toRegex()

    fun extractVideoIdFromUrl(url: String): String? {
        val matcher = videoIdRegex.findAll(url,0)
        var counter = 0
        for(item in matcher) {
            counter++
            if(counter == 1)
                return item.value
        }
        return null
    }

    fun validateYouTubeLink(url: String): Boolean {
        val compiledPattern = Pattern.compile(youTubeUrlRegEx)
        val matcher = compiledPattern.matcher(url)
        return matcher.find()
    }
}
