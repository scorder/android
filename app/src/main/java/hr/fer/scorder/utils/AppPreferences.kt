package hr.fer.scorder.utils

import android.content.Context
import hr.fer.scorder.entity.LoginResponse


class SharedPrefManager private constructor(private val ctx: Context) {

    val isLoggedIn: Boolean
        get() {
            val sharedPreferences = ctx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)
            return sharedPreferences.getString("email", "") != ""
        }

    val loginResponse: LoginResponse
        get() {
            val sharedPreferences = ctx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)
            return LoginResponse(
                sharedPreferences.getString("accessToken", null),
                sharedPreferences.getString("email", null),
                sharedPreferences.getString("tokenType", null)
            )
        }


    fun saveLoginResponse(loginResponse: LoginResponse) {

        val sharedPreferences = ctx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()

        editor.putString("accessToken", loginResponse.accessToken)
        editor.putString("email", loginResponse.email)
        editor.putString("tokenType", loginResponse.tokenType)

        editor.apply()

    }

    fun clear() {
        val sharedPreferences = ctx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.clear()
        editor.apply()
    }

    companion object {
        private val SHARED_PREF_NAME = "logged_user_data"
        private var mInstance: SharedPrefManager? = null
        @Synchronized
        fun getInstance(ctx: Context): SharedPrefManager {
            if (mInstance == null) {
                mInstance = SharedPrefManager(ctx)
            }
            return mInstance as SharedPrefManager
        }
    }
}