package hr.fer.scorder.utils

import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.ProgressBar

object ProgressBarController {

    fun enable(progressBar: ProgressBar, window: Window, view: View)  {
        progressBar.visibility = View.VISIBLE
        window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
        view.alpha = 0.5F
    }

    fun disable(progressBar: ProgressBar, window: Window, view: View)  {
        progressBar.visibility = View.GONE
        window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
        view.alpha = 1F
    }
}
