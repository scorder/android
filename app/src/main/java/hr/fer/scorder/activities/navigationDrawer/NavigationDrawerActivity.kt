package hr.fer.scorder.activities.navigationDrawer

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import hr.fer.scorder.R
import hr.fer.scorder.activities.homeScreen.HomeActivity
import hr.fer.scorder.activities.navigationDrawer.ui.CustomFragment
import hr.fer.scorder.activities.navigationDrawer.ui.currentTable.CurrentTableFragment
import hr.fer.scorder.activities.navigationDrawer.ui.listOfCaffes.NearbyCaffesFragment
import hr.fer.scorder.utils.SharedPrefManager
import com.google.android.material.navigation.NavigationView
import hr.fer.scorder.activities.navigationDrawer.ui.QRScanner.QRScannerFragment
import hr.fer.scorder.activities.navigationDrawer.ui.currentTable.TableInfo
import hr.fer.scorder.activities.navigationDrawer.ui.currentTable.currentTableInfo.atTheTable
import hr.fer.scorder.activities.navigationDrawer.ui.currentTable.currentTableInfo.leaveTable
import hr.fer.scorder.activities.navigationDrawer.ui.currentTable.currentTableInfo.tableInfo
import hr.fer.scorder.activities.navigationDrawer.ui.editUser.ProfileFragment
import hr.fer.scorder.net.RestFactory
import retrofit.Callback
import retrofit.RetrofitError
import retrofit.client.Response
import kotlin.reflect.KClass

class NavigationDrawerActivity : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var fragList: MutableList<CustomFragment>
    private lateinit var drawerLayout: DrawerLayout
    private lateinit var navView: NavigationView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_navigation_drawer)

        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        fragList = arrayListOf()
        drawerLayout = findViewById(R.id.drawer_layout)
        navView = findViewById(R.id.nav_view)
        val navController = findNavController(R.id.nav_host_fragment)

        appBarConfiguration = AppBarConfiguration(
                setOf(R.id.nav_empty_fragment, R.id.nav_nearby_caffes, R.id.nav_current_table, R.id.nav_edit_user_info, R.id.nav_QR_code), drawerLayout
        )

        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)

        atTheTable.observe(this, Observer {

            if(!it && tableInfo != null) {
                leaveTable(tableInfo!!.tableId, applicationContext)
                tableInfo = null
            }
            navView.menu.clear()
            supportFragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)

            if (it) {
                navView.inflateMenu(R.menu.second_menu)
                fragList.clear()

                setCurrentTableItem(navView.menu.findItem(R.id.nav_current_table))
                setNearbyCaffesItem(navView.menu.findItem(R.id.nav_nearby_caffes))

                navView.menu.performIdentifierAction(R.id.nav_current_table, 0)
                navView.setCheckedItem(R.id.nav_current_table)
            } else {
                navView.inflateMenu(R.menu.first_menu)
                navView.setCheckedItem(R.id.nav_nearby_caffes)
                fragList.clear()

                setScanQRCodeItem(navView.menu.findItem(R.id.nav_QR_code))
                setNearbyCaffesItem(navView.menu.findItem(R.id.nav_nearby_caffes))

                navView.menu.performIdentifierAction(R.id.nav_nearby_caffes, 0)
            }
            drawerLayout.closeDrawer(GravityCompat.START)
            setLogoutItem(navView.menu.findItem(R.id.nav_log_out))
            setEditUserInfoItem(navView.menu.findItem(R.id.nav_edit_user_info))
        })
    }

    override fun onResume() {
        super.onResume()
        val currentUserEmail = SharedPrefManager.getInstance(applicationContext).loginResponse.email
        navView.getHeaderView(0).findViewById<TextView>(R.id.currentUserEmailTextView).text = currentUserEmail
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    override fun onBackPressed() {
        val fragmentManager = supportFragmentManager
        if (fragList.size > 1) {
            val fragmentTransaction: FragmentTransaction = fragmentManager.beginTransaction()
            fragmentTransaction.remove(fragList.last())
            fragList.removeAt(fragList.size-1)
            fragmentTransaction.replace(R.id.nav_host_fragment, fragList.last())
            fragmentTransaction.commit()
            supportActionBar?.title = fragList.last().label
            fragList.last().itemId?.let { navView.setCheckedItem(it) }
        } else {
            super.onBackPressed()
        }
    }

    private fun setLogoutItem(logoutItem: MenuItem) {
        logoutItem.setOnMenuItemClickListener {

            atTheTable.value = false
            SharedPrefManager.getInstance(applicationContext).clear()
            val intent = Intent(this, HomeActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)

            true
        }
    }

    private fun setScanQRCodeItem(QRCodeItem: MenuItem) {
        setMenuItem(QRCodeItem, QRScannerFragment(), QRScannerFragment::class)
    }

    private fun setNearbyCaffesItem(nearbyCaffesItem: MenuItem) {
        setMenuItem(nearbyCaffesItem, NearbyCaffesFragment(), NearbyCaffesFragment::class)
    }

    private fun setCurrentTableItem(currentTableItem: MenuItem) {
        setMenuItem(currentTableItem, CurrentTableFragment(), CurrentTableFragment::class)
    }

    private fun setEditUserInfoItem(editUserInfoItem: MenuItem) {
        setMenuItem(editUserInfoItem, ProfileFragment(), ProfileFragment::class)
    }

    private fun <T: Any> setMenuItem(item: MenuItem, fragment: CustomFragment, clazz: KClass<T>) {
        val fragmentManager: FragmentManager = supportFragmentManager

        item.setOnMenuItemClickListener {

            if(fragmentManager.fragments.last().javaClass.simpleName != clazz.simpleName) {

                val fragmentTransaction: FragmentTransaction = fragmentManager.beginTransaction()
                fragList.add(fragment)
                fragmentTransaction.replace(R.id.nav_host_fragment, fragList.last())
                fragmentTransaction.commit()
                supportActionBar?.title = fragList.last().label

            }

            drawerLayout.closeDrawer(GravityCompat.START)
            true
        }
    }
}
