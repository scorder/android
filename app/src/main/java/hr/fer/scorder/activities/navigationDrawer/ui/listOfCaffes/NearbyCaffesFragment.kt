package hr.fer.scorder.activities.navigationDrawer.ui.listOfCaffes

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.MapsInitializer
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MapStyleOptions
import com.google.android.gms.maps.model.Marker
import hr.fer.scorder.R
import hr.fer.scorder.activities.caffeScreen.CaffeActivity
import hr.fer.scorder.activities.navigationDrawer.ui.CustomFragment
import hr.fer.scorder.activities.navigationDrawer.ui.listOfCaffes.NearbyCaffesAdapter.markerIds

class NearbyCaffesFragment : CustomFragment() {

    init {
        label = "Nearby caffe bars"
        itemId = R.id.nav_nearby_caffes
    }

    var mMapView: MapView? = null
    private var googleMap: GoogleMap? = null

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        super.onCreate(savedInstanceState)
        val root = inflater.inflate(R.layout.fragment_nearby_caffes, container, false)

        mMapView = root.findViewById(R.id.mapView)
        mMapView!!.onCreate(savedInstanceState)

        try {
            MapsInitializer.initialize(requireActivity().applicationContext)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        markerIds.clear()
        setupMap()

        return root
    }

    override fun onResume() {
        super.onResume()
        mMapView!!.onResume()
        mMapView!!.getMapAsync {
            NearbyCaffesFetcher.setNearbyCaffeMarkers(requireContext(), it, it.cameraPosition.target)
        }
    }

    override fun onPause() {
        super.onPause()

        try {
            googleMap!!.clear()
        } catch (ex: NullPointerException) {}
        markerIds.clear()
        try {
            mMapView!!.onPause()
        } catch (ex: NullPointerException) {}
    }

    override fun onDestroy() {
        super.onDestroy()
        mMapView!!.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mMapView!!.onLowMemory()
    }

    @SuppressLint("MissingPermission")
    private fun setupMap() {
        mMapView!!.getMapAsync { mMap ->
            googleMap = mMap
            googleMap!!.setMapStyle(MapStyleOptions.loadRawResourceStyle(context, R.raw.map_style))
            var cameraPosition: CameraPosition?

            googleMap!!.isMyLocationEnabled = true

            googleMap?.setInfoWindowAdapter(object: InfoWindowAdapter {
                override fun getInfoWindow(arg0: Marker): View? {
                    return null
                }

                override fun getInfoContents(marker: Marker): View {

                    val view = layoutInflater.inflate(R.layout.marker_info_window, null)

                    val cafeName = view.findViewById<TextView>(R.id.cafeNameTv)
                    val cafeAddress = view.findViewById<TextView>(R.id.cafeAddressTv)
                    val crowdDesc = view.findViewById<TextView>(R.id.crowdDescTv)
                    val greenCircle = view.findViewById<ImageView>(R.id.greenCircle)
                    val orangeCircle = view.findViewById<ImageView>(R.id.orangeCircle)
                    val redCircle = view.findViewById<ImageView>(R.id.redCircle)
                    val crowdPercent = (marker.tag as NearbyCaffesAdapter.CustomTag).crowdPercent

                    cafeName.text = marker.title
                    cafeAddress.text = marker.snippet
                    crowdDesc.text = crowdPercent.toString() + "% filled"
                    if(crowdPercent < 50) greenCircle.visibility = View.VISIBLE
                    else if(crowdPercent < 75) orangeCircle.visibility = View.VISIBLE
                    else redCircle.visibility = View.VISIBLE

                    return view
                }
            })

            LocationServices.getFusedLocationProviderClient(requireContext()).lastLocation.addOnSuccessListener {
                if(this@NearbyCaffesFragment.context != null) {
                    cameraPosition = CameraPosition.Builder().target(
                            LatLng(it.latitude, it.longitude)
                    ).zoom(16.5f).build()
                    googleMap!!.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
                    NearbyCaffesFetcher.setNearbyCaffeMarkers(this@NearbyCaffesFragment.requireContext(), googleMap!!, LatLng(it.latitude, it.longitude))
                }
            }

            googleMap!!.setOnCameraIdleListener {
                NearbyCaffesFetcher.setNearbyCaffeMarkers(requireContext(), googleMap!!, googleMap!!.cameraPosition.target)
            }

            googleMap!!.setOnInfoWindowClickListener {
                val intent = Intent(context, CaffeActivity::class.java)
                intent.putExtra("cafeName", it.title)
                intent.putExtra("cafeAddress", it.snippet)
                intent.putExtra("cafeId", (it.tag as NearbyCaffesAdapter.CustomTag).cafeId)
                ContextCompat.startActivity(requireContext(), intent, null)
            }
        }
    }
}

