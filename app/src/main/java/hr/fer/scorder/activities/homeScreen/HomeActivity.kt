package hr.fer.scorder.activities.homeScreen

import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.content.ContextCompat
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices
import hr.fer.scorder.R
import hr.fer.scorder.activities.loginScreen.LoginActivity
import hr.fer.scorder.activities.navigationDrawer.NavigationDrawerActivity
import hr.fer.scorder.activities.navigationDrawer.ui.currentTable.currentTableInfo.atTheTable
import hr.fer.scorder.activities.navigationDrawer.ui.currentTable.currentTableInfo.leaveTable
import hr.fer.scorder.activities.navigationDrawer.ui.currentTable.currentTableInfo.tableInfo
import hr.fer.scorder.activities.registerScreen.RegisterActivity
import hr.fer.scorder.entity.UserResponse
import hr.fer.scorder.net.RestFactory
import hr.fer.scorder.utils.ProgressBarController
import hr.fer.scorder.utils.SharedPrefManager
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.activity_home.progressBar
import kotlinx.android.synthetic.main.activity_login.*
import retrofit.Callback
import retrofit.RetrofitError
import retrofit.client.Response

const val LOCATION_REQUEST_CODE = 102

class HomeActivity : AppCompatActivity() {

    companion object {
        var locationTrackerStarted = false
        var waitingPermissionResponse = false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        supportActionBar?.title = ""

        if(!waitingPermissionResponse) {
            waitingPermissionResponse = true
            setupPermissionsAndTracker()
        }

        signInButton.setOnClickListener{
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
        }

        signUpButton.setOnClickListener{
            val intent = Intent(this, RegisterActivity::class.java)
            startActivity(intent)
        }
    }

    override fun onBackPressed() {
        if(progressBar.visibility == View.VISIBLE)
            return
        super.onBackPressed()
    }

    private fun loginCheck() {
        if(SharedPrefManager.getInstance(this).isLoggedIn) {

            RestFactory.instance(this).getCurrentUser(object : Callback<UserResponse?> {

                override fun success(userResponse: UserResponse?, response: Response?) {
                    ProgressBarController.disable(progressBar, window, homeLayout)
                    val intent = Intent(this@HomeActivity, NavigationDrawerActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    startActivity(intent)
                }

                override fun failure(error: RetrofitError?) {
                    ProgressBarController.disable(progressBar, window, homeLayout)
                    SharedPrefManager.getInstance(this@HomeActivity).clear()
                }

            } as Callback<UserResponse>)

        } else {
            ProgressBarController.disable(progressBar, window, homeLayout)
        }
    }

    @SuppressLint("MissingPermission")
    fun startLocationTracker() {
        val mLocationRequest = LocationRequest.create()
        mLocationRequest.interval = 10000
        mLocationRequest.fastestInterval = 4000
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        val mLocationCallback: LocationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult) {
                if(tableInfo != null) {
                    //TODO vratit pravu lokaciju posli a maknit testnu
                    val location = locationResult.lastLocation
                    val testLoc = Location("test")
                    testLoc.latitude = tableInfo!!.locationX!!
                    testLoc.longitude = tableInfo!!.locationY!!

                    val cafeLocation = Location("cafeInfo")
                    cafeLocation.latitude = tableInfo!!.locationX!!
                    cafeLocation.longitude = tableInfo!!.locationY!!
                    val result = location.distanceTo(cafeLocation)
                    if(result > 100) {
                        if(tableInfo != null) {
                            leaveTable(tableInfo!!.tableId, this@HomeActivity)
                            tableInfo = null
                            atTheTable.value = false
                            Toast.makeText(this@HomeActivity, "You are removed from table because you are too far from cafe.", Toast.LENGTH_LONG).show()
                        }
                    }
                }
            }
        }
        LocationServices.getFusedLocationProviderClient(this).requestLocationUpdates(mLocationRequest, mLocationCallback, null)
    }

    private fun setupPermissionsAndTracker() {
        ProgressBarController.enable(progressBar, window, homeLayout)

        val permission: Int = ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)

        if(permission != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(
                    arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                    LOCATION_REQUEST_CODE
            )
        } else {
            waitingPermissionResponse = false
            if(!locationTrackerStarted) {
                startLocationTracker()
                locationTrackerStarted = true
            }
            loginCheck()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            LOCATION_REQUEST_CODE -> {
                waitingPermissionResponse = false
                if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this, "You can\'t use ScOrder app without sharing your location.", Toast.LENGTH_LONG).show()
                    this.finishAffinity()
                } else {
                    if(!locationTrackerStarted) {
                        startLocationTracker()
                        locationTrackerStarted = true
                    }
                    loginCheck()
                }
            }
        }
    }
}

