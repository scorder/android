package hr.fer.scorder.activities.caffeScreen

import com.google.gson.annotations.SerializedName

class Event (

        @SerializedName("id") val id: Int,
        @SerializedName("eventName") val eventName: String,
        @SerializedName("date") val date: String,
        @SerializedName("ending") val ending: String,
        @SerializedName("eventInfo") val eventInfo: String
)
