package hr.fer.scorder.activities.navigationDrawer.ui.editUser

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import hr.fer.scorder.R
import hr.fer.scorder.entity.EditUserRequest
import hr.fer.scorder.entity.UserResponse
import hr.fer.scorder.net.RestFactory
import hr.fer.scorder.utils.ProgressBarController
import kotlinx.android.synthetic.main.activity_change_password.*
import kotlinx.android.synthetic.main.activity_login.cancelButton
import retrofit.Callback
import retrofit.RetrofitError
import retrofit.client.Response

class ChangePasswordActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_password)
        supportActionBar?.title = "Change password"

        cancelButton.setOnClickListener{
            this.onBackPressed()
        }

        confirmButton.setOnClickListener{

            val newPassword = newPasswordTV.text.toString().trim()
            val confirmNewPassword = confirmNewPasswordTV.text.toString().trim()

            if(newPassword != confirmNewPassword) {
                confirmNewPasswordTV.error = "Does not match with New Password"
                confirmNewPasswordTV.requestFocus()
                return@setOnClickListener
            }

            if(newPassword.length < 5){
                newPasswordTV.error = "Password must be at least 5 characters long"
                newPasswordTV.requestFocus()
                return@setOnClickListener
            }

            ProgressBarController.enable(progressBar, window, changePasswordLayout)

            val changedPasswordUser = EditUserRequest(null, null, null, null, newPassword)
            RestFactory.instance(this).editCurrentUser(changedPasswordUser, object : Callback<UserResponse?> {

                override fun success(userResponse: UserResponse?, response: Response?) {
                    ProgressBarController.disable(progressBar, window, changePasswordLayout)
                    Toast.makeText(this@ChangePasswordActivity, "Password changed successfully", Toast.LENGTH_SHORT).show()
                    this@ChangePasswordActivity.onBackPressed()
                }

                override fun failure(error: RetrofitError?) {
                    ProgressBarController.disable(progressBar, window, changePasswordLayout)
                    //TODO kako obradit ovu gresku???
                    Toast.makeText(this@ChangePasswordActivity, "Password change failed", Toast.LENGTH_SHORT).show()
                    this@ChangePasswordActivity.onBackPressed()
                }

            } as Callback<UserResponse>)
        }
    }

    override fun onBackPressed() {
        if(progressBar.visibility == View.VISIBLE)
            return
        super.onBackPressed()
    }
}
