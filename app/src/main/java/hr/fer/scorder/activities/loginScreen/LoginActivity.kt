package hr.fer.scorder.activities.loginScreen

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import hr.fer.scorder.R
import hr.fer.scorder.entity.LoginRequest
import hr.fer.scorder.entity.LoginResponse
import hr.fer.scorder.activities.homeScreen.HomeActivity
import hr.fer.scorder.activities.navigationDrawer.NavigationDrawerActivity
import hr.fer.scorder.net.RestFactory
import hr.fer.scorder.activities.registerScreen.RegisterActivity
import hr.fer.scorder.utils.ProgressBarController
import hr.fer.scorder.utils.SharedPrefManager
import kotlinx.android.synthetic.main.activity_login.*
import retrofit.Callback
import retrofit.RetrofitError
import retrofit.client.Response

class LoginActivity : AppCompatActivity() {

    var clickCounter: Int = 3

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        notRegisteredText.setOnClickListener {
            val intent = Intent(this, RegisterActivity::class.java)
            startActivity(intent)
        }

        loginButton.setOnClickListener {
            val email: String = emailEditText.text.toString().trim()
            val password: String = passwordEditText.text.toString().trim()

            if(email.isEmpty()) {
                emailEditText.error = "Email required!"
                emailEditText.requestFocus()
                return@setOnClickListener
            }

            if(password.isEmpty()) {
                passwordEditText.error = "Password required!"
                passwordEditText.requestFocus()
                return@setOnClickListener
            }


            login(LoginRequest(email, password))

        }

        cancelButton.setOnClickListener {
            clickCounter = 3
            loginButton.isEnabled = true
            val intent = Intent(this, HomeActivity::class.java)
            startActivity(intent)
        }
    }

    private fun login(loginRequest: LoginRequest) {

        ProgressBarController.enable(progressBar, window, loginLayout)

        RestFactory.instance(applicationContext).loginUser(loginRequest, object : Callback<LoginResponse?> {

            override fun success(loginResponse: LoginResponse?, response: Response?) {

                ProgressBarController.disable(progressBar, window, loginLayout)
                SharedPrefManager.getInstance(applicationContext).saveLoginResponse(loginResponse!!)
                val intent = Intent(applicationContext, NavigationDrawerActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)
            }

            override fun failure(error: RetrofitError?) {

                ProgressBarController.disable(progressBar, window, loginLayout)
                SharedPrefManager.getInstance(applicationContext).clear()
                clickCounter--
                val str: String = if (clickCounter == 1) " attempt left!" else " attempts left!"
                val text = "Wrong email or password!\n$clickCounter$str"
                Toast.makeText(applicationContext, text, Toast.LENGTH_SHORT).show()
                if(clickCounter == 0) loginButton.isEnabled = false
            }

        } as Callback<LoginResponse>)
    }

    override fun onBackPressed() {
        if(progressBar.visibility == View.VISIBLE)
            return
        super.onBackPressed()
    }

}
