package hr.fer.scorder.activities.navigationDrawer.ui.listOfCaffes

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Canvas
import android.location.Geocoder
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat.startActivity
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import hr.fer.scorder.R
import hr.fer.scorder.activities.caffeScreen.CaffeActivity
import java.util.*

object NearbyCaffesAdapter {

    val markerIds = mutableListOf<Long?>()

    fun setMarkers(context: Context, googleMap: GoogleMap, nearbyCaffes: List<CaffeMarkerData>) {
        for(caffe in nearbyCaffes) {
            if(!markerIds.contains(caffe.id)) {
                if(caffe.totalTables == 0) caffe.totalTables = 1
                googleMap.addMarker(
                        MarkerOptions()
                                .position(LatLng(caffe.latitude!!, caffe.longitude!!))
                                .title(caffe.name)
                                .snippet(caffe.address)
                                .icon(getBitmapFromView(context,caffe.name))
                ).tag = CustomTag(caffe.id!!, (caffe.takenTables!!.toFloat() / caffe.totalTables!! * 100).toInt())
                markerIds.add(caffe.id)
            }
        }
    }

    private fun getBitmapFromView(context: Context, title: String?): BitmapDescriptor? {
        val view: View = LayoutInflater.from(context)
                .inflate(R.layout.marker_costum, null)
        val titleTV = view.findViewById<TextView>(R.id.markerTitleTV)
        titleTV.text = title
        titleTV.measure(ConstraintLayout.LayoutParams.WRAP_CONTENT, ConstraintLayout.LayoutParams.WRAP_CONTENT)
        val markerImg = view.findViewById<ImageView>(R.id.markerImage)
        markerImg.measure(ConstraintLayout.LayoutParams.WRAP_CONTENT, ConstraintLayout.LayoutParams.WRAP_CONTENT)
        view.measure(ConstraintLayout.LayoutParams.WRAP_CONTENT, ConstraintLayout.LayoutParams.WRAP_CONTENT)

        var width: Int
        width = if(markerImg.measuredWidth > titleTV.measuredWidth)
            markerImg.measuredWidth
        else
            titleTV.measuredWidth
        val height = markerImg.measuredHeight + titleTV.measuredHeight
        val measuredWidth = View.MeasureSpec.makeMeasureSpec(width, View.MeasureSpec.EXACTLY)
        val measuredHeight = View.MeasureSpec.makeMeasureSpec(height, View.MeasureSpec.EXACTLY)
        view.measure(measuredWidth, measuredHeight)
        view.layout(0, 0, view.measuredWidth, view.measuredHeight)
        val bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        view.draw(canvas)
        return BitmapDescriptorFactory.fromBitmap(bitmap)
    }

    class CustomTag(val cafeId: Long, val crowdPercent: Int)
}

