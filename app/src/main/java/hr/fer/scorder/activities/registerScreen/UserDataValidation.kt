package hr.fer.scorder.activities.registerScreen

class UserDataValidation {

    fun isNameValid(name: String): Boolean {
        if(name.length < 1)
            return false
        return true
    }

    fun isEmailValid(email: String): Boolean {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    fun isPasswordValid(password: String): Boolean {
        if (password.length < 5)
            return false
        return true
    }

    fun isUsernameValid(username: String): Boolean {
        if(username.length < 1)
            return false
        return true
    }

}
