package hr.fer.scorder.activities.quizScreens

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import hr.fer.scorder.R
import hr.fer.scorder.activities.quizScreens.Quiz.numAllA
import hr.fer.scorder.activities.quizScreens.Quiz.numCorrectA
import nl.dionsegijn.konfetti.KonfettiView
import nl.dionsegijn.konfetti.models.Shape
import nl.dionsegijn.konfetti.models.Size


class ResultFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val thisView = inflater.inflate(R.layout.fragment_quiz_results, container,false)
        val ratingText: TextView = thisView.findViewById<TextView>(R.id.ratingTextView)
        ratingText.text = "Congratulations, you answered correct $numCorrectA/$numAllA!"
        val confetti: KonfettiView = (thisView.findViewById<View>(R.id.viewKonfetti) as KonfettiView)
        loadConfetti(confetti)
        return thisView
    }

    private fun loadConfetti(confetti: KonfettiView) {
        confetti.build()
            .addColors(Color.YELLOW, Color.GREEN, Color.MAGENTA)
            .setDirection(0.0, 359.0)
            .setSpeed(1f, 5f)
            .setFadeOutEnabled(true)
            .setTimeToLive(2000L)
            .addShapes(Shape.Square, Shape.Circle)
            .addSizes(Size(12))
            .setPosition(-50f, confetti.width + 50f, -50f, -50f)
            .streamFor(300, 5000L)
    }
}