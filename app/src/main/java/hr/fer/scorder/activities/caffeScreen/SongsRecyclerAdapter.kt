package hr.fer.scorder.activities.caffeScreen

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import hr.fer.scorder.R
import hr.fer.scorder.entity.SongResponse
import kotlinx.android.synthetic.main.songs_item.view.*

class SongsRecyclerAdapter(private val songList: List<SongResponse>) : RecyclerView.Adapter<SongsRecyclerAdapter.SongsViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SongsViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.songs_item, parent, false)
        return SongsViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: SongsViewHolder, position: Int) {
        val currentItem = songList[position]
        holder.songName.text = if(!currentItem.name.equals("")) currentItem.name else "/"
        holder.songArtist.text = if(!currentItem.artist.equals("")) currentItem.artist else "/"
        holder.album.text = if(!currentItem.album.equals("")) currentItem.album else "/"
        holder.rbr.text = ""
    }


    override fun getItemCount() = songList.size

    class SongsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val songName: TextView = itemView.songNameTextView
        val songArtist: TextView = itemView.songArtistTextView
        val album: TextView = itemView.albumTextView
        val rbr: TextView = itemView.rbrTV
    }

}
