package hr.fer.scorder.activities.registerScreen

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import hr.fer.scorder.R
import hr.fer.scorder.entity.LoginRequest
import hr.fer.scorder.entity.LoginResponse
import hr.fer.scorder.entity.RegisterUserRequest
import hr.fer.scorder.activities.loginScreen.LoginActivity
import hr.fer.scorder.activities.navigationDrawer.NavigationDrawerActivity
import hr.fer.scorder.net.RestFactory
import hr.fer.scorder.utils.ProgressBarController
import hr.fer.scorder.utils.SharedPrefManager
import kotlinx.android.synthetic.main.activity_register.*
import retrofit.Callback
import retrofit.RetrofitError
import retrofit.client.Response

class RegisterActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        registerButton.setOnClickListener {

            val firstName: String = etFirstName.text.toString()
            val lastName: String = etLastName.text.toString()
            val username: String = etUsername.text.toString()
            val email: String = etEmail.text.toString()
            val password: String = etPassword.text.toString()
            val isFirstNameValid: Boolean = UserDataValidation().isNameValid(firstName)
            val isLastNameValid: Boolean = UserDataValidation().isNameValid(lastName)
            val isUsernameValid: Boolean = UserDataValidation().isUsernameValid(username)
            val isEmailValid: Boolean = UserDataValidation().isEmailValid(email)
            val isPasswordValid: Boolean = UserDataValidation().isPasswordValid(password)

            val duration = Toast.LENGTH_SHORT

            if(isFirstNameValid && isLastNameValid && isEmailValid && isPasswordValid && isUsernameValid) {
                register(RegisterUserRequest(firstName, lastName, username, email, password))
            } else {
                if(!isFirstNameValid){
                    val text = "Enter first name"
                    val t = Toast.makeText(applicationContext, text, duration)
                    t.show()
                } else if(!isLastNameValid) {
                    val text = "Enter last name"
                    val t = Toast.makeText(applicationContext, text, duration)
                    t.show()
                } else if(!isUsernameValid) {
                    val text = "Enter username"
                    val t = Toast.makeText(applicationContext, text, duration)
                    t.show()
                } else if (!isEmailValid){
                    val text = "Email is invalid"
                    val t = Toast.makeText(applicationContext, text, duration)
                    t.show()
                } else {
                    val text = "Password must be at least 5 characters long"
                    val t = Toast.makeText(applicationContext, text, duration)
                    t.show()
                }
            }
        }
    }

    private fun register(newUser: RegisterUserRequest) {

        ProgressBarController.enable(progressBar, window, registerLayout)

        RestFactory.instance(applicationContext).registerUser(newUser, object : Callback<Void?> {

            override fun success(registerResponse: Void?, response: Response?) {

                Toast.makeText(applicationContext, "Successfully registered", Toast.LENGTH_SHORT).show()
                login(LoginRequest(newUser.email, newUser.password))
            }

            override fun failure(error: RetrofitError?) {

                ProgressBarController.disable(progressBar, window, registerLayout)
                Toast.makeText(applicationContext, "Registration failed", Toast.LENGTH_SHORT).show()
            }
        })
    }

    private fun login(loginRequest: LoginRequest) {

        RestFactory.instance(applicationContext).loginUser(loginRequest, object : Callback<LoginResponse?> {

            override fun success(loginResponse: LoginResponse?, response: Response?) {

                ProgressBarController.disable(progressBar, window, registerLayout)
                SharedPrefManager.getInstance(applicationContext).saveLoginResponse(loginResponse!!)
                val intent = Intent(applicationContext, NavigationDrawerActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)
            }

            override fun failure(error: RetrofitError?) {

                ProgressBarController.disable(progressBar, window, registerLayout)
                SharedPrefManager.getInstance(applicationContext).clear()
                val text = "Wrong email or password!"
                Toast.makeText(applicationContext, text, Toast.LENGTH_SHORT).show()
                val intent = Intent(applicationContext, LoginActivity::class.java)
                startActivity(intent)
            }

        } as Callback<LoginResponse>)
    }

    override fun onBackPressed() {
        if(progressBar.visibility == View.VISIBLE)
            return
        super.onBackPressed()
    }
}
