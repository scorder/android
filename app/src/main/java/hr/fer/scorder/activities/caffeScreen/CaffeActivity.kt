package hr.fer.scorder.activities.caffeScreen

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import hr.fer.scorder.R
import kotlinx.android.synthetic.main.activity_caffe.*
import java.lang.NullPointerException

class CaffeActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_caffe)

        val bundle = intent.extras ?: throw NullPointerException("No extras passed to CafeActivity.")
        val cafeId = bundle.getLong("cafeId")
        val cafeName = bundle.getString("cafeName")
        val cafeAddress = bundle.getString("cafeAddress")

        tvCaffeName.text = cafeName
        tvCaffeAddress.text = cafeAddress

        beverageListButton.setOnClickListener{
            val intent = Intent(this, DrinksActivity::class.java)
            intent.putExtra("cafeId", cafeId)
            startActivity(intent)
        }

        songListButton.setOnClickListener{
            val intent = Intent(this, SongsActivity::class.java)
            intent.putExtra("cafeId", cafeId)
            startActivity(intent)
        }

        eventListButton.setOnClickListener{
            val intent = Intent(this, EventsActivity::class.java)
            intent.putExtra("cafeId", cafeId)
            startActivity(intent)
        }

    }

}
