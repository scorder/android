package hr.fer.scorder.activities.quizScreens

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import hr.fer.scorder.R
import hr.fer.scorder.activities.navigationDrawer.NavigationDrawerActivity
import hr.fer.scorder.activities.navigationDrawer.ui.currentTable.currentTableInfo
import hr.fer.scorder.activities.quizScreens.Quiz.answers
import hr.fer.scorder.activities.quizScreens.Quiz.cancel
import hr.fer.scorder.activities.quizScreens.Quiz.eventId
import hr.fer.scorder.activities.quizScreens.Quiz.on
import hr.fer.scorder.activities.quizScreens.Quiz.questions
import hr.fer.scorder.activities.quizScreens.Quiz.quizAvailable
import hr.fer.scorder.activities.quizScreens.Quiz.resultsAvailable
import hr.fer.scorder.activities.quizScreens.Quiz.timer
import hr.fer.scorder.entity.Answers
import hr.fer.scorder.entity.PlaceAnswers
import hr.fer.scorder.net.RestFactory
import kotlinx.android.synthetic.main.activity_quiz_question.*
import retrofit.Callback
import retrofit.RetrofitError
import retrofit.client.Response


class QuizQuestionActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_quiz_question)

        var flag = false

        val bundle = intent.extras
        var tableId: Long? = null
        if(bundle != null) {
            tableId = bundle.getLong("tableId")
        } else {
            throw Exception("No tableId passed to QuizQuestionActivity as extra!")
        }

        currentTableInfo.tableInfo?.quizSolvedBefore  = true

        if(quizAvailable) {

            if(on == -1) {
                getQuestions(eventId)
            }

            buttonAnswer.setOnClickListener {
                if(on < questions.size - 1) {
                    timer(buttonAnswer, on)
                    on++
                    showQuestionFragment()
                } else {
                    if(flag) {
                        val intent = Intent(this, NavigationDrawerActivity::class.java)
                        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                        startActivity(intent)
                    } else {
                        timer(buttonAnswer, on)
                        on++
                        sendAnswers(tableId)
                    }
                    flag = true
                }
            }
        } else if(resultsAvailable) {
            buttonAnswer.text = "End"
            buttonAnswer. setOnClickListener {
                val intent = Intent(this, NavigationDrawerActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)
            }
            showPlacementsFragment()
        }


    }

    private fun showQuestionFragment() {
        val transaction = supportFragmentManager.beginTransaction()
        val fragment = QuestionFragment()
        transaction.replace(R.id.fragmentHolder, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    private fun showResultFragment() {
        val transaction = supportFragmentManager.beginTransaction()
        val fragment = ResultFragment()
        transaction.replace(R.id.fragmentHolder, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    private fun showPlacementsFragment() {
        val transaction = supportFragmentManager.beginTransaction()
        val fragment = PlacementsFragment()
        transaction.replace(R.id.fragmentHolder, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    private fun getQuestions(eventId: Long?) {

        if(eventId == null) return

        RestFactory.instance(applicationContext).getQuestions(eventId, object :
            Callback<List<Question>> {

            override fun success(questionsResponse: List<Question>, response: Response?) {
                questions = questionsResponse as MutableList<Question>
                on = 0
                timer(buttonAnswer, on)
                showQuestionFragment()
            }

            override fun failure(error: RetrofitError?) {
                val text = "Error with getting questions"
                Toast.makeText(applicationContext, text, Toast.LENGTH_SHORT).show()
            }

        } as Callback<List<Question>>)
    }

    private fun sendAnswers(tableId: Long?) {

        if(tableId == null || eventId == null) return

        val dataList = mutableListOf<Answers>()

        for ((k, v) in answers) {
            dataList.add(Answers(k, v))
        }

        var placeAnswers = PlaceAnswers(tableId.toString(), eventId.toString(), dataList)

        RestFactory.instance(applicationContext).sendAnswers(placeAnswers, object :
            Callback<List<Int>> {

            override fun success(resultsResponse: List<Int>, response: Response?) {
                buttonAnswer.text = "End"
                cancel(resultsResponse[0], resultsResponse[1])
                showResultFragment()
            }

            override fun failure(error: RetrofitError?) {
                val text = "Error with getting results"
                Toast.makeText(applicationContext, text, Toast.LENGTH_SHORT).show()
            }

        } as Callback<List<Int>>)
    }

    override fun onBackPressed() {}
}
