package hr.fer.scorder.activities.quizScreens

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import hr.fer.scorder.R
import hr.fer.scorder.entity.PlacementsResponse

class PlacementsViewHolder(inflater: LayoutInflater, parent: ViewGroup) :
        RecyclerView.ViewHolder(inflater.inflate(R.layout.placement_item_layout, parent, false)) {

    private var itemPlacement: TextView? = null
    private var itemUsername: TextView? = null
    private var itemPercentage: TextView? = null


    init {
        itemPlacement = itemView.findViewById(R.id.placementTextView)
        itemUsername = itemView.findViewById(R.id.usernameTextView)
        itemPercentage = itemView.findViewById(R.id.percentageTextView)}

    fun bind(user: PlacementsResponse) {
        if(user.place == -1) {
            itemPlacement?.text = "Placement"
            itemUsername?.text = user.username
            itemPercentage?.text = "Percentage"
            return
        }
        itemPlacement?.text = user.place.toString() + "."
        itemUsername?.text = user.username
        itemPercentage?.text = user.percentage.toString() + "%"
    }

}