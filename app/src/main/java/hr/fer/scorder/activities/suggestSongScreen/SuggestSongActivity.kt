package hr.fer.scorder.activities.suggestSongScreen

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import hr.fer.scorder.R
import hr.fer.scorder.entity.SuggestSongRequest
import hr.fer.scorder.net.RestFactory
import hr.fer.scorder.utils.ProgressBarController
import kotlinx.android.synthetic.main.activity_suggest_song.*
import retrofit.Callback
import retrofit.RetrofitError
import retrofit.client.Response

class SuggestSongActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_suggest_song)
        supportActionBar?.title = "Suggest new song"

        val cafeId = intent.extras!!.getLong("cafeId")

        resetButton.setOnClickListener {
            songNameEditText.setText("")
            artistEditText.setText("")
            albumEditText.setText("")
            linkEditText.setText("")
        }

        submitButton.setOnClickListener {

            if(songNameEditText.text.toString().trim() == "") {
                Toast.makeText(this, "Song name is empty.", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if(artistEditText.text.toString().trim() == "") {
                Toast.makeText(this, "Artist name is empty.", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if(!YouTubeHelper.validateYouTubeLink(linkEditText.text.toString().trim())) {
                Toast.makeText(this, "Youtube link is not valid.", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            val song = SuggestSongRequest(cafeId, songNameEditText.text.toString().trim(),
                    artistEditText.text.toString().trim(), albumEditText.text.toString().trim(),
                    YouTubeHelper.extractVideoIdFromUrl(linkEditText.text.toString().trim())
            )

            if(song.link == null) {
                Toast.makeText(this, "Youtube link is not valid.", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            ProgressBarController.enable(progressBar, window, suggestLayout)
            RestFactory.instance(this).suggestSong(song, object : Callback<Void?> {

                override fun success(res: Void?, response: Response?) {
                    ProgressBarController.disable(progressBar, window, suggestLayout)
                    if(response?.status == 208) {
                        Toast.makeText(this@SuggestSongActivity, "There is already suggestion same as yours.", Toast.LENGTH_LONG).show()
                    } else if(response?.status == 202) {
                        Toast.makeText(this@SuggestSongActivity, "Your suggestion is successfully received.", Toast.LENGTH_LONG).show()
                    }
                    this@SuggestSongActivity.onBackPressed()
                }

                override fun failure(error: RetrofitError?) {
                    ProgressBarController.disable(progressBar, window, suggestLayout)
                    if(error?.response!!.status == 400) {
                        Toast.makeText(this@SuggestSongActivity, "Cafe already has this song in it's list of songs.", Toast.LENGTH_LONG).show()
                    }
                    this@SuggestSongActivity.onBackPressed()
                }

            } as Callback<Void?>)

        }
    }
}