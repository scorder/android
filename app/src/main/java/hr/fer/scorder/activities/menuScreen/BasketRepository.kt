package hr.fer.scorder.activities.menuScreen

object BasketRepository {
    var basket = mutableListOf<MenuItemModel>()
    var counts = mutableListOf<Int>()
    var basketCheckout = mutableListOf<MenuItemModel>()
    var countsCheckout = mutableListOf<Int>()
    var flag = false

    fun addItem(menu: List<MenuItemModel>, position: Int) {
        if(!flag) {
            flag = true
            for(i in 0..(menu.size - 1)) {
                basket.add(i, menu[i])
                counts.add(i, 0)
            }
        }
        counts[position] += 1
    }

    fun substractItem(menu: List<MenuItemModel>, position: Int) {
        if(!flag) {
            flag = true
            for(i in 0..(menu.size - 1)) {
                basket.add(i, menu[i])
                counts.add(i, 0)
            }
        }
        if(counts[position] == 0) return
        counts[position] -= 1
    }

    fun sum() : Float {
        var sum: Float = 0.0f
        for(i in 0..(basket.size - 1)) {
            sum += (basket[i].price!! * counts[i])
        }
        return sum
    }

    fun checkoutItems() {
        for(i in 0..(basket!!.size - 1)) {
            if(counts[i] > 0) {
                basketCheckout.add(basket[i])
                countsCheckout.add(counts[i])
            }
        }
    }

    fun clearItems() {
        basketCheckout.clear()
        countsCheckout.clear()
        basket.clear()
        counts.clear()
        flag = false
    }

}
