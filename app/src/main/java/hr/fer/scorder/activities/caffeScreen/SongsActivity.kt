package hr.fer.scorder.activities.caffeScreen

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import hr.fer.scorder.R
import hr.fer.scorder.entity.SongResponse
import hr.fer.scorder.net.RestFactory
import hr.fer.scorder.utils.ProgressBarController
import kotlinx.android.synthetic.main.activity_songs.*
import retrofit.Callback
import retrofit.RetrofitError
import retrofit.client.Response

class SongsActivity : AppCompatActivity() {
    val search = MutableLiveData<String>().apply { value = ""}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_songs)
        supportActionBar?.title = "Songs"

        val cafeId = intent.extras!!.getLong("cafeId")
        var songs: List<SongResponse>?

        ProgressBarController.enable(progressBar, window, songsLayout)
        RestFactory.instance(applicationContext).getSongs(cafeId, object : Callback<List<SongResponse>> {

            override fun success(songsResponse: List<SongResponse>?, response: Response?) {
                ProgressBarController.disable(progressBar, window, songsLayout)

                songs = songsResponse
                val itemDecorator = DividerItemDecoration(this@SongsActivity, DividerItemDecoration.VERTICAL)
                itemDecorator.setDrawable(ContextCompat.getDrawable(this@SongsActivity, R.drawable.divider)!!)

                search.observe(this@SongsActivity, Observer {
                    var list = songs
                    if(it != "")
                        list = songs!!.filter { song -> song.artist?.contains(it, true)!! ||
                                song.name?.contains(it, true)!! ||
                                song.album?.contains(it, true)!!
                        }

                    songs_recycler_view.adapter = SongsRecyclerAdapter(list!!)
                    songs_recycler_view.layoutManager = LinearLayoutManager(this@SongsActivity)
                    songs_recycler_view.setHasFixedSize(true)
                    songs_recycler_view.addItemDecoration(itemDecorator)
                })
            }

            override fun failure(error: RetrofitError?) {
                ProgressBarController.disable(progressBar, window, songsLayout)
                //TODO bacit gresku ili poslat neku poruku
                throw error!!
            }

        } as Callback<List<SongResponse>>)

        songSearchButton.setOnClickListener {
            search.value = songSearchEditText.text.toString()
        }
    }

    override fun onBackPressed() {
        if(progressBar.visibility == View.VISIBLE)
            return
        super.onBackPressed()
    }
}
