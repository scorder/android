package hr.fer.scorder.activities.orderSongScreen

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.CompoundButton
import android.widget.TextView
import hr.fer.scorder.R
import hr.fer.scorder.entity.SongResponse


class SongListAdapter(val applicationContext: Context,
                      private val songResponses: List<SongResponse>,
                      private val selectedSongIds: List<Long>
                      ): BaseAdapter() {

    val inflater = (LayoutInflater.from(applicationContext))

    override fun getCount(): Int {
        return songResponses.size
    }

    override fun getItem(position: Int): Any {
        return songResponses[position]
    }

    override fun getItemId(position: Int): Long {
        return songResponses[position].id!!
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view = inflater.inflate(R.layout.songs_item, null)

        val songName = view.findViewById<TextView>(R.id.songNameTextView)
        songName.text = if(!songResponses[position].name.equals("")) songResponses[position].name else "/"

        val artist = view.findViewById<TextView>(R.id.songArtistTextView)
        artist.text = if(!songResponses[position].artist.equals("")) songResponses[position].artist else "/"

        val album = view.findViewById<TextView>(R.id.albumTextView)
        album.text = if(!songResponses[position].album.equals("")) songResponses[position].album else "/"

        val rbr = view.findViewById<TextView>(R.id.rbrTV)
        rbr.text = ""

        if(selectedSongIds.contains(songResponses[position].id))
            view.setBackgroundColor(Color.parseColor("#E6E6FA"))
        return view;
    }

}
