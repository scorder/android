package hr.fer.scorder.activities.navigationDrawer.ui.currentTable

import android.content.Context
import androidx.lifecycle.MutableLiveData
import hr.fer.scorder.net.RestFactory
import retrofit.Callback
import retrofit.RetrofitError
import retrofit.client.Response

object currentTableInfo {
    val atTheTable = MutableLiveData<Boolean>().apply { value = false }
    var tableInfo: TableInfo? = null

    fun leaveTable(tableId: Long, context: Context) {
        RestFactory.instance(context).leave(tableId, object : Callback<Void?> {

            override fun success(void: Void?, response: Response?) {

            }

            override fun failure(error: RetrofitError?) {

            }

        } as Callback<Void?>)
    }
}
