package hr.fer.scorder.activities.quizScreens

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import hr.fer.scorder.R
import hr.fer.scorder.activities.menuScreen.PlacementsRecyclerAdapter
import hr.fer.scorder.activities.quizScreens.Quiz.usersPlacement
import hr.fer.scorder.entity.PlacementsResponse
import kotlinx.android.synthetic.main.fragment_quiz_placements.*

class PlacementsFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_quiz_placements, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        var users = mutableListOf<PlacementsResponse>()
        users.add(PlacementsResponse(-1, "Username", -1))
        users.addAll(usersPlacement!!)
        placements_recycler_view.apply {
            layoutManager = LinearLayoutManager(activity)
            adapter = PlacementsRecyclerAdapter(users)
        }
    }

    companion object {
        fun newInstance(): PlacementsFragment = PlacementsFragment()
    }
}
