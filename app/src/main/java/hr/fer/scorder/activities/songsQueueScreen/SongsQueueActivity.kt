package hr.fer.scorder.activities.songsQueueScreen

import android.graphics.Color
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import hr.fer.scorder.R
import hr.fer.scorder.entity.SongResponse
import hr.fer.scorder.net.RestFactory
import hr.fer.scorder.utils.ProgressBarController
import kotlinx.android.synthetic.main.activity_songs_queue.*
import retrofit.Callback
import retrofit.RetrofitError
import retrofit.client.Response
import java.lang.NullPointerException

class SongsQueueActivity: AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_songs_queue)
        supportActionBar?.title = "Songs queue"

        val bundle = intent.extras ?: throw NullPointerException("No extras passed to SongsQueueActivity.")
        val cafeId = bundle.getLong("cafeId")

        ProgressBarController.enable(progressBar, window, songsQueueLayout)
        RestFactory.instance(applicationContext).getSongsQueue(cafeId, object : Callback<List<SongResponse>> {

            override fun success(songsResponse: List<SongResponse>?, response: Response?) {
                ProgressBarController.disable(progressBar, window, songsQueueLayout)

                val itemDecorator = DividerItemDecoration(this@SongsQueueActivity, DividerItemDecoration.VERTICAL)
                itemDecorator.setDrawable(ContextCompat.getDrawable(this@SongsQueueActivity, R.drawable.divider)!!)

                queue_recycler_view.adapter = QueueAdapter(songsResponse!!)
                queue_recycler_view.layoutManager = LinearLayoutManager(this@SongsQueueActivity)
                queue_recycler_view.setHasFixedSize(true)
                queue_recycler_view.addItemDecoration(itemDecorator)

                if(songsResponse.isEmpty()) {
                    emptyQueueTV.setTextColor(Color.RED)
                    emptyQueueTV.text = "There is no songs in queue."
                } else {
                    emptyQueueTV.visibility = View.GONE
                }
            }

            override fun failure(error: RetrofitError?) {
                ProgressBarController.disable(progressBar, window, songsQueueLayout)
                //TODO bacit gresku ili poslat neku poruku
                throw error!!
            }

        } as Callback<List<SongResponse>>)
    }

    override fun onBackPressed() {
        if(progressBar.visibility == View.VISIBLE)
            return
        super.onBackPressed()
    }
}
