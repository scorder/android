package hr.fer.scorder.activities.menuScreen

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import hr.fer.scorder.activities.menuScreen.BasketRepository

class CountsViewModel : ViewModel() {
    var counts = MutableLiveData<List<Int>>()

    fun getCounts() {
        counts.value = BasketRepository.counts
    }
}
