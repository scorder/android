package hr.fer.scorder.activities.menuScreen

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import hr.fer.scorder.R
import hr.fer.scorder.activities.checkoutScreen.CheckoutActivity
import hr.fer.scorder.activities.menuScreen.BasketRepository.basketCheckout
import hr.fer.scorder.activities.menuScreen.BasketRepository.checkoutItems
import hr.fer.scorder.activities.menuScreen.BasketRepository.countsCheckout
import hr.fer.scorder.net.RestFactory
import hr.fer.scorder.utils.ProgressBarController
import kotlinx.android.synthetic.main.activity_menu.*
import retrofit.Callback
import retrofit.RetrofitError
import retrofit.client.Response

class MenuActivity : AppCompatActivity() {

    lateinit var summaryViewModel: SummaryViewModel
    lateinit var countsViewModel: CountsViewModel
    lateinit var menu: List<MenuItemModel>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)

        val bundle = intent.extras
        var cafeId: Long?
        var tableId: Long?
        if(bundle != null) {
            cafeId = bundle.getLong("cafeId")
            tableId = bundle.getLong("tableId")
        } else {
            throw Exception("No cafeId or tableId passed to MenuActivity as extra!")
        }


        getBeverages(cafeId)

        checkoutButton.setOnClickListener {
            basketCheckout.clear()
            countsCheckout.clear()
            checkoutItems()
            val intent = Intent(this, CheckoutActivity::class.java)
            intent.putExtra("tableId", tableId)
            startActivity(intent)
        }
    }

    private fun getBeverages(caffeId: Long?) {

        if(caffeId == null) return

        ProgressBarController.enable(progressBar, window, menuLayout)
        RestFactory.instance(applicationContext).getMenu(caffeId, object :
            Callback<List<MenuItemModel>> {

            override fun success(menuResponse: List<MenuItemModel>, response: Response?) {
                ProgressBarController.disable(progressBar, window, menuLayout)
                menu = menuResponse

                menu_recycler_view.layoutManager = LinearLayoutManager(applicationContext)

                summaryViewModel =
                    ViewModelProvider(this@MenuActivity, ViewModelProvider.AndroidViewModelFactory(application)).get(
                        SummaryViewModel::class.java)

                countsViewModel =
                    ViewModelProvider(this@MenuActivity, ViewModelProvider.AndroidViewModelFactory(application)).get(
                        CountsViewModel::class.java)

                val adapter = MenuRecyclerAdapter(menu, summaryViewModel, countsViewModel)
                menu_recycler_view.adapter = adapter


                val summaryObserver = Observer<String> { newSum ->
                    priceSummaryText.text = newSum
                }


                countsViewModel.counts.observe(this@MenuActivity, Observer {
                    adapter.notifyDataSetChanged()
                })

                summaryViewModel.summary.observe(this@MenuActivity, summaryObserver)

            }

            override fun failure(error: RetrofitError?) {
                ProgressBarController.disable(progressBar, window, menuLayout)
                val text = "Error with getting beverages"
                Toast.makeText(applicationContext, text, Toast.LENGTH_SHORT).show()
            }

        } as Callback<List<MenuItemModel>>)
    }

    override fun onResume() {
        super.onResume()
        //TODO moze li se ovo izbrisat
        //countsViewModel.getCounts()
        //summaryViewModel.getSummary()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed();
        return true;
    }
}
