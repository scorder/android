package hr.fer.scorder.activities.navigationDrawer.ui.currentTable

class TableInfo (
        var tableId: Long,
        var tableNumber: Int,
        var cafeId: Long,
        var cafeName: String,
        var cafeAddress: String,
        var locationX: Double,
        var locationY: Double,
        var availableSongOrders: Int = 0,
        var quizSolvedBefore: Boolean = false
)
