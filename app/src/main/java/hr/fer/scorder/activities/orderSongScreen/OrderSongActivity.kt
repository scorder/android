package hr.fer.scorder.activities.orderSongScreen

import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import hr.fer.scorder.R
import hr.fer.scorder.activities.navigationDrawer.ui.currentTable.currentTableInfo.tableInfo
import hr.fer.scorder.entity.ExcludedSongsResponse
import hr.fer.scorder.entity.OrderSongRequest
import hr.fer.scorder.entity.SongResponse
import hr.fer.scorder.net.RestFactory
import hr.fer.scorder.utils.ProgressBarController
import kotlinx.android.synthetic.main.activity_order_song.*
import retrofit.Callback
import retrofit.RetrofitError
import retrofit.client.Response
import java.lang.NullPointerException

class OrderSongActivity : AppCompatActivity() {
    private val search = MutableLiveData<String>().apply { value = ""}
    var selectedSongIds = ArrayList<Long>()
    var excludedSongs: ExcludedSongsResponse? = null
    var songs: List<SongResponse>? = null
    private val availableOrders = MutableLiveData<Int>().apply { value = 0}
    @Volatile
    var waitingHttpResponses = 2

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_order_song)

        supportActionBar?.title = "OrderEntry song"

        val bundle = intent.extras ?: throw NullPointerException("No extras passed to OrderSongActivity.")
        val cafeId = bundle.getLong("cafeId")

        ProgressBarController.enable(progressBar, window, orderSongLayout)
        getExcludedSongIds(cafeId)
        getSongsAndSetListView(cafeId)

        if(tableInfo == null) onBackPressed()
        availableOrders.value = tableInfo?.availableSongOrders!!
        availableOrders.observe(this, Observer {
            availableOrdersTV.setTextColor(Color.BLACK)
            if(it == 1)
                availableOrdersTV.text = "You can select " + it + " more song."
            else if(it == 0) {
                availableOrdersTV.setTextColor(Color.RED)
                availableOrdersTV.text = "You can\'t select any more songs."
            } else
                availableOrdersTV.text = "You can select " + it + " more songs."
        })

        val tableId = bundle.getLong("tableId")
        orderSongButton.setOnClickListener {
            if(selectedSongIds.isEmpty()) {
                Toast.makeText(applicationContext, "Select at least one song to order.", Toast.LENGTH_SHORT).show()
            } else {
                val orderSongRequest = OrderSongRequest(tableId, selectedSongIds.toLongArray())
                ProgressBarController.enable(progressBar, window, orderSongLayout)
                RestFactory.instance(applicationContext).orderSong(orderSongRequest, object : Callback<Void?> {

                    override fun success(orderResponse: Void?, response: Response?) {
                        ProgressBarController.disable(progressBar, window, orderSongLayout)
                        Toast.makeText(this@OrderSongActivity, "OrderEntry is successfully received.", Toast.LENGTH_LONG).show()
                        this@OrderSongActivity.onBackPressed()
                    }

                    override fun failure(error: RetrofitError?) {
                        ProgressBarController.disable(progressBar, window, orderSongLayout)
                        //TODO bacit gresku ili poslat neku poruku
                        throw error!!
                    }

                } as Callback<Void?>)
            }
        }

        songSearchButton.setOnClickListener {
            search.value = songSearchEditText.text.toString()
        }
    }

    private fun getSongsAndSetListView(cafeId: Long) {

        RestFactory.instance(applicationContext).getSongs(cafeId, object : Callback<List<SongResponse>> {

            override fun success(songsResponse: List<SongResponse>?, response: Response?) {
                songs = songsResponse
                waitingHttpResponses--
                if(waitingHttpResponses == 0) {
                    setSearchObserve()
                    ProgressBarController.disable(progressBar, window, orderSongLayout)
                }
            }

            override fun failure(error: RetrofitError?) {
                ProgressBarController.disable(progressBar, window, orderSongLayout)
                //TODO bacit gresku ili poslat neku poruku
                throw error!!
            }

        } as Callback<List<SongResponse>>)
    }

    private fun getExcludedSongIds(cafeId: Long) {
        RestFactory.instance(applicationContext).getExcludedSongs(cafeId, object : Callback<ExcludedSongsResponse> {

            override fun success(resp: ExcludedSongsResponse?, response: Response?) {
                excludedSongs = resp
                waitingHttpResponses--
                if(waitingHttpResponses == 0) {
                    setSearchObserve()
                    ProgressBarController.disable(progressBar, window, orderSongLayout)
                }
            }

            override fun failure(error: RetrofitError?) {
                ProgressBarController.disable(progressBar, window, orderSongLayout)
                //TODO bacit gresku ili poslat neku poruku
                throw error!!
            }

        } as Callback<ExcludedSongsResponse>)
    }

    private fun setSearchObserve() {
        search.observe(this@OrderSongActivity, Observer {
            var list = songs!!
            if(it != "")
                list = songs!!.filter { song -> song.artist?.contains(it, true)!! ||
                        song.name?.contains(it, true)!! ||
                        song.album?.contains(it, true)!!
                }

            songListView.adapter = SongListAdapter(applicationContext, list, selectedSongIds)
            songListView.setOnItemClickListener { parent, view, position, id ->

                val songId = list[position].id!!

                if(selectedSongIds.contains(songId)) {
                    selectedSongIds.remove(songId)
                    view.setBackgroundColor(Color.WHITE)
                    availableOrders.value = availableOrders.value!! + 1
                    return@setOnItemClickListener
                }

                if(availableOrders.value == 0 && selectedSongIds.size == 0) {
                    Toast.makeText(this@OrderSongActivity, "You can\'t select any song. Please order more drinks before ordering songs.", Toast.LENGTH_SHORT).show()
                    return@setOnItemClickListener
                }

                if(availableOrders.value == 0) {
                    Toast.makeText(this@OrderSongActivity, "You can\'t select any more songs. Please unselect some song or order already selected songs.", Toast.LENGTH_SHORT).show()
                    return@setOnItemClickListener
                }


                if(excludedSongs!!.prev!!.contains(songId)) {
                    Toast.makeText(this@OrderSongActivity, "Can't select this song. It was in last 5 played songs.", Toast.LENGTH_SHORT).show()
                    return@setOnItemClickListener
                }

                if(excludedSongs!!.fut!!.contains(songId)) {
                    Toast.makeText(this@OrderSongActivity, "Can't select this song. It is already in queue.", Toast.LENGTH_SHORT).show()
                    return@setOnItemClickListener
                }

                selectedSongIds.add(songId)
                view.setBackgroundColor(Color.parseColor("#E6E6FA"))
                availableOrders.value = availableOrders.value!! - 1
            }
        })
    }

    override fun onBackPressed() {
        if(progressBar.visibility == View.VISIBLE)
            return
        super.onBackPressed()
    }
}
