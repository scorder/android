package hr.fer.scorder.activities.quizScreens

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class QuizAvailabilityViewModel : ViewModel() {
    var quizA = MutableLiveData<Boolean>()

    fun getQuizAvailability() {
        quizA.value = Quiz.quizAvailable
    }
}
