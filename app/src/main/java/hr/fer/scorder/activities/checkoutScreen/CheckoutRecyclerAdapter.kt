package hr.fer.scorder.activities.checkoutScreen

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import hr.fer.scorder.R
import hr.fer.scorder.activities.menuScreen.*
import kotlinx.android.synthetic.main.checkout_item_layout.view.*
import kotlinx.android.synthetic.main.checkout_item_layout.view.servingSize

class CheckoutRecyclerAdapter (private var checkout: List<MenuItemModel>?, summaryViewModel: SummaryViewModel, countsViewModel: CountsViewModel) : RecyclerView.Adapter<CheckoutRecyclerAdapter.CheckoutViewHolder>() {

    inner  class CheckoutViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var itemTitle: TextView? = null
        var servingSize: TextView? = null
        var itemPrice: TextView? = null
        var itemCount: TextView? = null

        init {
             itemTitle = itemView.beverageName
             servingSize = itemView.servingSize
             itemPrice = itemView.beveragePrice
             itemCount = itemView.beverageCount
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CheckoutViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)
        val menuElement = inflater.inflate(R.layout.checkout_item_layout, parent, false)
        return CheckoutViewHolder(menuElement)

    }

    override fun getItemCount(): Int {
        if(checkout != null) return checkout!!.size
        else return 0
    }

    override fun onBindViewHolder(holder: CheckoutViewHolder, position: Int) {
        if(checkout != null) {
            holder.itemTitle?.text = checkout!![position].name
            holder.servingSize?.text = checkout!![position].servingSize.toString() + " " + checkout!![position].unitOfMeasure
            holder.itemPrice?.text = checkout!![position].price.toString()
            holder.itemCount?.text = BasketRepository.countsCheckout[position].toString()
        }
    }

}
