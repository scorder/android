package hr.fer.scorder.activities.quizScreens

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class ResultsAvailabilityViewModel : ViewModel() {
    var resultsA = MutableLiveData<Boolean>()

    fun getResultsAvailability() {
        resultsA.value = Quiz.resultsAvailable
    }
}
