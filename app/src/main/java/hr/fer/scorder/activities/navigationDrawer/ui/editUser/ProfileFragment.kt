package hr.fer.scorder.activities.navigationDrawer.ui.editUser

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import hr.fer.scorder.R
import hr.fer.scorder.activities.loginScreen.LoginActivity
import hr.fer.scorder.activities.navigationDrawer.NavigationDrawerActivity
import hr.fer.scorder.activities.navigationDrawer.ui.CustomFragment
import hr.fer.scorder.activities.navigationDrawer.ui.currentTable.currentTableInfo.atTheTable
import hr.fer.scorder.entity.UserResponse
import hr.fer.scorder.net.RestFactory
import hr.fer.scorder.utils.ProgressBarController
import hr.fer.scorder.utils.SharedPrefManager
import kotlinx.android.synthetic.main.activity_navigation_drawer.*
import retrofit.Callback
import retrofit.RetrofitError
import retrofit.client.Response

class ProfileFragment : CustomFragment() {

    init {
        label = "Profile"
        itemId = R.id.nav_edit_user_info
    }

    val user = MutableLiveData<UserResponse>()
    var progressBar: ProgressBar? = null
    var root: View? = null

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        super.onCreate(savedInstanceState)
        root = inflater.inflate(R.layout.fragment_profile, container, false)

        progressBar = root!!.findViewById(R.id.profileProgressBar)

        val firstNameTv = root!!.findViewById<TextView>(R.id.userFirstName)
        val lastNameTv = root!!.findViewById<TextView>(R.id.userLastName)
        val usernameTv = root!!.findViewById<TextView>(R.id.username)
        val emailTv = root!!.findViewById<TextView>(R.id.userEmail)
        user.observe(viewLifecycleOwner, Observer {
            firstNameTv.text = it.firstName
            lastNameTv.text = it.lastName
            usernameTv.text = it.username
            emailTv.text = it.email
        })

        val changePasswordButton = root!!.findViewById<Button>(R.id.changePasswordButton)
        changePasswordButton.setOnClickListener{
            val intent = Intent(activity, ChangePasswordActivity::class.java)
            startActivity(intent)
        }

        val editButton = root!!.findViewById<Button>(R.id.editButton)
        editButton.setOnClickListener{
            val intent = Intent(activity, EditProfileActivity::class.java)
            intent.putExtra("FirstName", user.value?.firstName)
            intent.putExtra("LastName", user.value?.lastName)
            intent.putExtra("Username", user.value?.username)
            intent.putExtra("Email", user.value?.email)
            startActivity(intent)
        }

        return root
    }

    override fun onResume() {
        getCurrentUser()
        super.onResume()
    }

    private fun getCurrentUser() {
        ProgressBarController.enable(progressBar!!, requireActivity().window, root!!)

        RestFactory.instance(requireContext()).getCurrentUser(object : Callback<UserResponse?> {

            override fun success(userResponse: UserResponse?, response: Response?) {
                ProgressBarController.disable(progressBar!!, requireActivity().window, root!!)
                user.value = userResponse
            }

            override fun failure(error: RetrofitError?) {
                ProgressBarController.disable(progressBar!!, requireActivity().window, root!!)
                logout()
            }

        } as Callback<UserResponse>)
    }

    private fun logout() {
        SharedPrefManager.getInstance(requireContext()).clear()
        val intent = Intent(requireContext(), LoginActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        atTheTable.value = false
        Toast.makeText(context, "Failed to get current user. Please sign in again.", Toast.LENGTH_LONG).show()
    }
}
