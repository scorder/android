package hr.fer.scorder.activities.navigationDrawer.ui.editUser

import android.content.Intent
import android.os.Bundle
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import hr.fer.scorder.R
import hr.fer.scorder.activities.homeScreen.HomeActivity
import hr.fer.scorder.activities.navigationDrawer.ui.currentTable.currentTableInfo.atTheTable
import hr.fer.scorder.activities.registerScreen.UserDataValidation
import hr.fer.scorder.entity.EditUserRequest
import hr.fer.scorder.entity.LoginRequest
import hr.fer.scorder.entity.LoginResponse
import hr.fer.scorder.entity.UserResponse
import hr.fer.scorder.net.RestFactory
import hr.fer.scorder.utils.ProgressBarController
import hr.fer.scorder.utils.SharedPrefManager
import kotlinx.android.synthetic.main.activity_edit_profile.*
import retrofit.Callback
import retrofit.RetrofitError
import retrofit.client.Response

class EditProfileActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_profile)
        supportActionBar?.title = "Edit profile"

        val extras = intent.extras
        if(extras == null) {
            throw IllegalAccessException()
        } else {
            firstNameET.setText(extras.getString("FirstName"))
            lastNameET.setText(extras.getString("LastName"))
            usernameET.setText(extras.getString("Username"))
            emailET.setText(extras.getString("Email"))
        }

        updateButton.setOnClickListener{
            if(!validateData()) {
                return@setOnClickListener
            }

            val editedUser = EditUserRequest(
                    firstNameET.text.toString().trim(),
                    lastNameET.text.toString().trim(),
                    emailET.text.toString().trim(),
                    usernameET.text.toString().trim(),
                    null
            )

            ProgressBarController.enable(progressBar, window, editProfileLayout)
            RestFactory.instance(this).editCurrentUser(editedUser, object : Callback<UserResponse?> {

                override fun success(userResponse: UserResponse?, response: Response?) {
                    ProgressBarController.disable(progressBar, window, editProfileLayout)
                    if (!userResponse?.email.equals(extras.getString("Email"))) {
                        SharedPrefManager.getInstance(applicationContext).clear()
                        showLoginDialog(userResponse?.email)
                    } else {
                        Toast.makeText(this@EditProfileActivity, "User updated", Toast.LENGTH_SHORT).show()
                        this@EditProfileActivity.onBackPressed()
                    }
                }

                override fun failure(error: RetrofitError?) {
                    ProgressBarController.disable(progressBar, window, editProfileLayout)
                    errorTextView.text = "Email or username already in use by another user."
                }

            } as Callback<UserResponse>)
        }
    }

    private fun validateData(): Boolean {
        val validator = UserDataValidation()

        if(!validator.isNameValid(firstNameET.text.toString().trim())) {
            firstNameET.error = "First name required"
            firstNameET.requestFocus()
            return false
        }

        if(!validator.isNameValid(lastNameET.text.toString().trim())) {
            lastNameET.error = "Last name required"
            lastNameET.requestFocus()
            return false
        }

        if(!validator.isUsernameValid(usernameET.text.toString().trim())) {
            usernameET.error = "Username required"
            usernameET.requestFocus()
            return false
        }

        if(!validator.isEmailValid(emailET.text.toString().trim())) {
            emailET.error = "Wrong email format"
            emailET.requestFocus()
            return false
        }

        return true
    }

    private fun showLoginDialog(email: String?) {

        val dialogView = layoutInflater.inflate(R.layout.dialog_login, null)
        val emailET = dialogView.findViewById<EditText>(R.id.emailEditText)
        emailET.setText(email)
        emailET.isEnabled = false

        val builder = AlertDialog.Builder(this@EditProfileActivity)
        builder.setTitle("Sign in")
                .setView(dialogView)
                .setPositiveButton("Confirm"
                ) { dialog, id -> }
                .setNegativeButton("Cancel"
                ) { dialog, id ->
                    logout()
                }

        val dialog = builder.create()
        dialog.show()
        val posButton = dialog.getButton(AlertDialog.BUTTON_POSITIVE)
        posButton.setOnClickListener {
            val passwordET = dialogView.findViewById<EditText>(R.id.passwordEditText)
            login(email, passwordET, dialog)
        }
    }

    private fun login(email: String?, passwordET: EditText, dialog: AlertDialog) {
        val loginRequest = LoginRequest(email, passwordET.text.toString())
        ProgressBarController.enable(progressBar, window, editProfileLayout)
        RestFactory.instance(applicationContext).loginUser(loginRequest, object : Callback<LoginResponse?> {

            override fun success(loginResponse: LoginResponse?, response: Response?) {
                ProgressBarController.disable(progressBar, window, editProfileLayout)

                dialog.dismiss()
                SharedPrefManager.getInstance(applicationContext).saveLoginResponse(loginResponse!!)
                Toast.makeText(this@EditProfileActivity, "User updated", Toast.LENGTH_SHORT).show()
                this@EditProfileActivity.onBackPressed()
            }

            override fun failure(error: RetrofitError?) {
                ProgressBarController.disable(progressBar, window, editProfileLayout)

                passwordET.error = "Wrong password! Try again."
                passwordET.requestFocus()
            }

        } as Callback<LoginResponse>)
    }

    private fun logout() {
        SharedPrefManager.getInstance(applicationContext).clear()
        val intent = Intent(this, HomeActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        atTheTable.value = false
    }
}
