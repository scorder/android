package hr.fer.scorder.activities.navigationDrawer.ui.listOfCaffes

import android.content.Context
import android.location.Geocoder
import android.widget.Toast
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.LatLng
import hr.fer.scorder.entity.CaffeResponse
import hr.fer.scorder.entity.NearbyCaffesRequest
import hr.fer.scorder.net.RestFactory
import retrofit.Callback
import retrofit.RetrofitError
import retrofit.client.Response
import java.util.*

object NearbyCaffesFetcher {


    fun setNearbyCaffeMarkers(context: Context, googleMap: GoogleMap, location: LatLng) {
        val bounds = googleMap.projection.visibleRegion.latLngBounds
        val nearbyCaffesRequest = NearbyCaffesRequest(
                location.latitude,
                location.longitude,
                bounds.northeast.latitude - location.latitude,
                bounds.northeast.longitude - location.longitude
        )

        RestFactory.instance(context).nearbyCaffes(nearbyCaffesRequest, object : Callback<List<CaffeResponse>> {

            override fun success(nearbyCaffes: List<CaffeResponse>?, response: Response?) {
                val geocoder = Geocoder(context, Locale.getDefault())
                NearbyCaffesAdapter.setMarkers(
                        context,
                        googleMap,
                        nearbyCaffes?.map { caffe ->
                            CaffeMarkerData(
                                    caffe.id,
                                    caffe.name,
                                    geocoder.getFromLocation(caffe.locationX!!, caffe.locationY!!, 1).get(0).getAddressLine(0),
                                    caffe.locationX,
                                    caffe.locationY,
                                    caffe.takenTables,
                                    caffe.totalTables
                            )
                        }!!
                )
            }

            override fun failure(error: RetrofitError?) {
                //TODO bacit gresku ili poslat neku poruku
                throw error!!
            }

        } as Callback<List<CaffeResponse>>)
    }

}

