package hr.fer.scorder.activities.caffeScreen

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import hr.fer.scorder.R
import hr.fer.scorder.activities.menuScreen.MenuItemModel
import hr.fer.scorder.net.RestFactory
import hr.fer.scorder.utils.ProgressBarController
import kotlinx.android.synthetic.main.activity_drinks.*
import retrofit.Callback
import retrofit.RetrofitError
import retrofit.client.Response

class DrinksActivity : AppCompatActivity() {

    lateinit var menu: List<MenuItemModel>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_drinks)
        supportActionBar?.title = "Beverages"

        val bundle = intent.extras
        var cafeId: Long?
        if(bundle != null) {
            cafeId = bundle.getLong("cafeId")
        } else {
            throw Exception("No cafeId passed to DrinksActivity as extra!")
        }

        getBeverages(cafeId)

    }

    private fun getBeverages(cafeId: Long?) {

        if(cafeId == null) return

        ProgressBarController.enable(progressBar, window, drinksLayout)
        RestFactory.instance(applicationContext).getMenu(cafeId, object :
                Callback<List<MenuItemModel>> {

            override fun success(menuResponse: List<MenuItemModel>, response: Response?) {
                ProgressBarController.disable(progressBar, window, drinksLayout)
                menu = menuResponse
                checkout_recycler_view.adapter = DrinksRecyclerAdapter(menu)
                checkout_recycler_view.layoutManager = LinearLayoutManager(applicationContext)
                checkout_recycler_view.setHasFixedSize(true)
            }

            override fun failure(error: RetrofitError?) {
                ProgressBarController.disable(progressBar, window, drinksLayout)
                val text = "Error with getting beverages"
                Toast.makeText(applicationContext, text, Toast.LENGTH_SHORT).show()
            }

        } as Callback<List<MenuItemModel>>)
    }

}
