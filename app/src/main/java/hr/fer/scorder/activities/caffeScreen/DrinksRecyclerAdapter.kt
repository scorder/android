package hr.fer.scorder.activities.caffeScreen

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import hr.fer.scorder.R
import hr.fer.scorder.activities.menuScreen.MenuItemModel
import kotlinx.android.synthetic.main.drinks_item_layout.view.*

class DrinksRecyclerAdapter(private val drinkList: List<MenuItemModel>) : RecyclerView.Adapter<DrinksRecyclerAdapter.DrinksViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DrinksViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.drinks_item_layout, parent, false)
        return DrinksViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: DrinksViewHolder, position: Int) {
        val currentItem = drinkList[position]
        holder.drinkName.text = currentItem.name
        holder.servingSize.text = currentItem.servingSize.toString() + " " + currentItem.unitOfMeasure
        holder.drinkPrice.text = currentItem.price.toString()
        holder.drinkCurrency.text = "HRK"
    }


    override fun getItemCount() = drinkList.size

    class DrinksViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val drinkName: TextView = itemView.caffeDrinkName
        val servingSize: TextView = itemView.servingSize
        val drinkPrice: TextView = itemView.caffeDrinkPrice
        val drinkCurrency: TextView = itemView.caffeDrinkCurrency
    }

}
