package hr.fer.scorder.activities.menuScreen

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import hr.fer.scorder.R
import kotlinx.android.synthetic.main.menu_item_layout.view.*

class MenuRecyclerAdapter(private var menu: List<MenuItemModel>?, summaryViewModel: SummaryViewModel, countsViewModel: CountsViewModel) : RecyclerView.Adapter<MenuRecyclerAdapter.ViewHolder>() {

    var summaryModel: SummaryViewModel = summaryViewModel
    var countsModel: CountsViewModel = countsViewModel

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var itemTitle: TextView? = null
        var servingSize: TextView? = null
        var itemPrice: TextView? = null
        var itemCount: TextView? = null

        init {
            itemTitle = itemView.menuItemTitle
            servingSize = itemView.servingSize
            itemPrice = itemView.menuItemPrice
            itemCount = itemView.menuItemCount

            itemView.addButton.setOnClickListener {
                BasketRepository.addItem(menu!!, adapterPosition)
                countsModel.getCounts()
                summaryModel.getSummary()
            }

            itemView.substractButton.setOnClickListener {
                BasketRepository.substractItem(menu!!, adapterPosition)
                countsModel.getCounts()
                summaryModel.getSummary()
            }
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)
        val menuElement = inflater.inflate(R.layout.menu_item_layout, parent, false)
        return ViewHolder(menuElement)

    }

    override fun getItemCount(): Int {
        if(menu != null) return menu!!.size
        else return 0
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        if(menu != null) {
            viewHolder.itemTitle?.text = menu!![position].name
            viewHolder.servingSize?.text = menu!![position].servingSize.toString() + " " + menu!![position].unitOfMeasure
            viewHolder.itemPrice?.text = menu!![position].price.toString()
            if(BasketRepository.counts.size == 0) viewHolder.itemCount?.text = "0"
            else viewHolder.itemCount?.text = BasketRepository.counts[position].toString()
        }
    }
}
