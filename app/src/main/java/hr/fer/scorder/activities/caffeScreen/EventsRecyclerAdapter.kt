package hr.fer.scorder.activities.caffeScreen

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import hr.fer.scorder.R
import kotlinx.android.synthetic.main.events_item_layout.view.*

class EventsRecyclerAdapter (private val eventList: List<Event>) : RecyclerView.Adapter<EventsRecyclerAdapter.EventsViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EventsViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.events_item_layout, parent, false)
        return EventsViewHolder(itemView)
    }

    val eventName: TextView? = null
    val date: TextView? = null
    val hour: TextView? = null

    class EventsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val eventName = itemView.eventNameTextView
        val date = itemView.dateTextView
        val hour = itemView.eventTimeTextView
    }

    override fun getItemCount() = eventList.size

    override fun onBindViewHolder(holder: EventsViewHolder, position: Int) {
        val currentItem = eventList[position]
        holder.eventName.text = currentItem.eventName
        holder.date.text = currentItem.date.substring(0, 10)
        holder.hour.text = currentItem.date.substring(11, 19)
    }

}
