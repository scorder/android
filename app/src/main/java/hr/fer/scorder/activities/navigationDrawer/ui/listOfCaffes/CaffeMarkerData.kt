package hr.fer.scorder.activities.navigationDrawer.ui.listOfCaffes

class CaffeMarkerData(
    var id: Long? = null,
    var name: String? = null,
    var address: String? = null,
    var latitude: Double? = null,
    var longitude: Double? = null,
    var takenTables: Int? = null,
    var totalTables: Int? = null
)
