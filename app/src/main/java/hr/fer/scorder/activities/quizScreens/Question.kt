package hr.fer.scorder.activities.quizScreens

import com.google.gson.annotations.SerializedName

open class Question (
    @SerializedName("question") val question: String,
    @SerializedName("answerA") val answerA: String,
    @SerializedName("answerB") val answerB: String,
    @SerializedName("answerC") val answerC: String,
    @SerializedName("answerD") val answerD: String
)
