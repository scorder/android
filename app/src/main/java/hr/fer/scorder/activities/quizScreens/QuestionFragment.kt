package hr.fer.scorder.activities.quizScreens

import android.graphics.Color
import androidx.fragment.app.Fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import hr.fer.scorder.R
import hr.fer.scorder.activities.quizScreens.Quiz.answers
import hr.fer.scorder.activities.quizScreens.Quiz.counter
import hr.fer.scorder.activities.quizScreens.Quiz.on
import hr.fer.scorder.activities.quizScreens.Quiz.questions
import kotlinx.coroutines.*


class QuestionFragment : Fragment() {

    var timeInS = 20L
    lateinit var thisView: View

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        thisView = inflater.inflate(R.layout.fragment_quiz_question,container,false)

        ++counter


        GlobalScope.launch {
            withContext(Dispatchers.IO) {
                while (timeInS > 0) {
                    timeInS--
                    withContext(Dispatchers.Main) {
                        thisView.findViewById<TextView>(R.id.secondsLeftTextView).setText(timeInS.toString())
                        if (timeInS<= 10) {
                            thisView.findViewById<TextView>(R.id.secondsLeftTextView).setTextColor(Color.RED)
                        }
                    }
                    delay(1000)
                }
            }
        }

        val txt = (on + 1).toString() + ". pitanje"
        thisView.findViewById<TextView>(R.id.questionMetaTextView).text = txt
        thisView.findViewById<TextView>(R.id.questionTextView).text = questions[on].question
        thisView.findViewById<TextView>(R.id.answerATextView).text = questions[on].answerA
        thisView.findViewById<TextView>(R.id.answerBTextView).text = questions[on].answerB
        thisView.findViewById<TextView>(R.id.answerCTextView).text = questions[on].answerC
        thisView.findViewById<TextView>(R.id.answerDTextView).text = questions[on].answerD

        answers[questions[on].question] = " "

        thisView.findViewById<TextView>(R.id.answerATextView).setOnClickListener {
            answers[questions[on].question] = thisView.findViewById<TextView>(R.id.answerATextView).text.toString()
            it.setBackgroundColor(Color.parseColor("#D1CDCD"))
            thisView.findViewById<TextView>(R.id.answerBTextView).setBackgroundColor(Color.WHITE)
            thisView.findViewById<TextView>(R.id.answerCTextView).setBackgroundColor(Color.WHITE)
            thisView.findViewById<TextView>(R.id.answerDTextView).setBackgroundColor(Color.WHITE)
        }

        thisView.findViewById<TextView>(R.id.answerBTextView).setOnClickListener {
            answers[questions[on].question] = thisView.findViewById<TextView>(R.id.answerBTextView).text.toString()
            it.setBackgroundColor(Color.parseColor("#D1CDCD"))
            thisView.findViewById<TextView>(R.id.answerATextView).setBackgroundColor(Color.WHITE)
            thisView.findViewById<TextView>(R.id.answerCTextView).setBackgroundColor(Color.WHITE)
            thisView.findViewById<TextView>(R.id.answerDTextView).setBackgroundColor(Color.WHITE)
        }

        thisView.findViewById<TextView>(R.id.answerCTextView).setOnClickListener {
            answers[questions[on].question] = thisView.findViewById<TextView>(R.id.answerCTextView).text.toString()
            it.setBackgroundColor(Color.parseColor("#D1CDCD"))
            thisView.findViewById<TextView>(R.id.answerATextView).setBackgroundColor(Color.WHITE)
            thisView.findViewById<TextView>(R.id.answerBTextView).setBackgroundColor(Color.WHITE)
            thisView.findViewById<TextView>(R.id.answerDTextView).setBackgroundColor(Color.WHITE)
        }

        thisView.findViewById<TextView>(R.id.answerDTextView).setOnClickListener {
            answers[questions[on].question] = thisView.findViewById<TextView>(R.id.answerDTextView).text.toString()
            it.setBackgroundColor(Color.parseColor("#D1CDCD"))
            thisView.findViewById<TextView>(R.id.answerATextView).setBackgroundColor(Color.WHITE)
            thisView.findViewById<TextView>(R.id.answerBTextView).setBackgroundColor(Color.WHITE)
            thisView.findViewById<TextView>(R.id.answerCTextView).setBackgroundColor(Color.WHITE)
        }

        return thisView
    }
}
