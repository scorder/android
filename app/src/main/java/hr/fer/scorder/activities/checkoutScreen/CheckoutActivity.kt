package hr.fer.scorder.activities.checkoutScreen

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import hr.fer.scorder.R
import hr.fer.scorder.activities.menuScreen.*
import hr.fer.scorder.activities.menuScreen.BasketRepository.basketCheckout
import hr.fer.scorder.activities.menuScreen.BasketRepository.clearItems
import hr.fer.scorder.activities.menuScreen.BasketRepository.countsCheckout
import hr.fer.scorder.activities.menuScreen.BasketRepository.sum
import hr.fer.scorder.activities.navigationDrawer.NavigationDrawerActivity
import hr.fer.scorder.activities.navigationDrawer.ui.currentTable.currentTableInfo.atTheTable
import hr.fer.scorder.activities.navigationDrawer.ui.currentTable.currentTableInfo.tableInfo
import hr.fer.scorder.entity.OrderEntry
import hr.fer.scorder.entity.PlaceOrderRequest
import hr.fer.scorder.net.RestFactory
import kotlinx.android.synthetic.main.activity_checkout.checkoutButton
import kotlinx.android.synthetic.main.activity_checkout.checkout_recycler_view
import kotlinx.android.synthetic.main.activity_checkout.priceSummaryText
import retrofit.Callback
import retrofit.RetrofitError
import retrofit.client.Response

class CheckoutActivity : AppCompatActivity() {

    lateinit var summaryViewModel: SummaryViewModel
    lateinit var countsViewModel: CountsViewModel
    lateinit var checkout: List<MenuItemModel>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_checkout)

        val bundle = intent.extras
        var tableId: Long?
        if(bundle != null) {
            tableId = bundle.getLong("tableId")
        } else {
            throw Exception("No tableId passed to CheckoutActivity as extra!")
        }

        checkout = basketCheckout

        checkout_recycler_view.layoutManager = LinearLayoutManager(this)

        summaryViewModel =
                ViewModelProvider(this, ViewModelProvider.AndroidViewModelFactory(application)).get(
                        SummaryViewModel::class.java)

        countsViewModel =
                ViewModelProvider(this, ViewModelProvider.AndroidViewModelFactory(application)).get(
                        CountsViewModel::class.java)

        val adapter = CheckoutRecyclerAdapter(checkout, summaryViewModel, countsViewModel)
        checkout_recycler_view.adapter = adapter


        priceSummaryText.text = sum().toString()

        checkoutButton.setOnClickListener(){
            if(basketCheckout.isEmpty()) {
                val text = "You didn\'t select any beverage."
                val t = Toast.makeText(applicationContext, text, Toast.LENGTH_SHORT)
                t.show()
                return@setOnClickListener
            }
            placeOrder(tableId)
        }
    }

    private fun placeOrder(tableId: Long?) {

        if(tableId == null) return

        val dataList = mutableListOf<OrderEntry>()
        for(i in 0 until basketCheckout.size) {
            dataList.add(OrderEntry(basketCheckout[i].id, countsCheckout[i]))
        }

        var placeOrderRequest = PlaceOrderRequest(tableId.toString(), dataList)

        if(atTheTable.value!!) {
            RestFactory.instance(applicationContext).placeOrder(placeOrderRequest, object :
                    Callback<Void?> {

                override fun success(t: Void?, response: Response?) {
                    val text = "Order placed successfully"
                    Toast.makeText(applicationContext, text, Toast.LENGTH_SHORT).show()

                    var counter = 0
                    for(num in countsCheckout)
                        counter += num
                    tableInfo!!.availableSongOrders += counter * 2

                    clearItems()
                    val intent = Intent(this@CheckoutActivity, NavigationDrawerActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    startActivity(intent)
                }

                override fun failure(error: RetrofitError?) {
                    val text = "Error with placing an order"
                    Toast.makeText(applicationContext, text, Toast.LENGTH_SHORT).show()
                }

            } as Callback<Void?>)
        } else {
            val text = "Error with placing an order"
            Toast.makeText(applicationContext, text, Toast.LENGTH_SHORT).show()
        }
    }
}
