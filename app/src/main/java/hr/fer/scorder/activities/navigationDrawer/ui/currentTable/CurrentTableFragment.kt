package hr.fer.scorder.activities.navigationDrawer.ui.currentTable

import android.content.ContentValues.TAG
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import hr.fer.scorder.R
import hr.fer.scorder.activities.caffeScreen.CaffeActivity
import hr.fer.scorder.activities.menuScreen.MenuActivity
import hr.fer.scorder.activities.navigationDrawer.NavigationDrawerActivity
import hr.fer.scorder.activities.navigationDrawer.ui.CustomFragment
import hr.fer.scorder.activities.navigationDrawer.ui.currentTable.currentTableInfo.atTheTable
import hr.fer.scorder.activities.navigationDrawer.ui.currentTable.currentTableInfo.tableInfo
import hr.fer.scorder.activities.orderSongScreen.OrderSongActivity
import hr.fer.scorder.activities.quizScreens.Quiz.eventId
import hr.fer.scorder.activities.quizScreens.Quiz.initialisePlacements
import hr.fer.scorder.activities.quizScreens.Quiz.quizAvailable
import hr.fer.scorder.activities.quizScreens.Quiz.resultsAvailable
import hr.fer.scorder.activities.quizScreens.QuizAvailabilityViewModel
import hr.fer.scorder.activities.quizScreens.QuizQuestionActivity
import hr.fer.scorder.activities.quizScreens.ResultsAvailabilityViewModel
import hr.fer.scorder.activities.songsQueueScreen.SongsQueueActivity
import hr.fer.scorder.activities.suggestSongScreen.SuggestSongActivity
import hr.fer.scorder.entity.EventResponse
import hr.fer.scorder.entity.PlacementsResponse
import hr.fer.scorder.net.RestFactory
import kotlinx.coroutines.*
import retrofit.Callback
import retrofit.RetrofitError
import retrofit.client.Response
import java.lang.Math.floor
import java.sql.Timestamp
import java.text.SimpleDateFormat
import java.time.Duration
import java.time.temporal.ChronoUnit


class CurrentTableFragment : CustomFragment() {

    init {
        label = "Current table"
        itemId = R.id.nav_current_table
    }

    lateinit var quizViewModel: QuizAvailabilityViewModel
    lateinit var resultsViewModel: ResultsAvailabilityViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        try {
            super.onCreate(savedInstanceState)
            val root = inflater.inflate(R.layout.fragment_current_table, container, false)

            val barNameTextView = root.findViewById<TextView>(R.id.barNameTextView)
            barNameTextView.text = tableInfo?.cafeName
            val tableTextView = root.findViewById<TextView>(R.id.tableTextView)
            tableTextView.text = "Table " + tableInfo?.tableNumber

            quizViewModel =
                    ViewModelProvider(this, ViewModelProvider.AndroidViewModelFactory(requireActivity().application)).get(
                            QuizAvailabilityViewModel::class.java)

            resultsViewModel =
                    ViewModelProvider(this, ViewModelProvider.AndroidViewModelFactory(requireActivity().application)).get(
                            ResultsAvailabilityViewModel::class.java)

            val eventButton = root.findViewById<Button>(R.id.eventButton)

            val quizObserver = Observer<Boolean> { newVal ->
                if(newVal) {
                    eventButton.text = "Enter quiz"
                    eventButton.isEnabled = true
                }
            }

            val resultsObserver = Observer<Boolean> { newVal ->
                if(newVal) {
                    eventButton.text = "Placements"
                    eventButton.isEnabled = true
                }
            }

            quizViewModel.quizA.observe(viewLifecycleOwner, quizObserver)
            resultsViewModel.resultsA.observe(viewLifecycleOwner, resultsObserver)


            val infoButton = root.findViewById<Button>(R.id.showBarInfoButton)
            infoButton.setOnClickListener{
                val intent = Intent(activity, CaffeActivity::class.java)
                intent.putExtra("cafeId", tableInfo?.cafeId)
                intent.putExtra("cafeName", tableInfo?.cafeName)
                intent.putExtra("cafeAddress", tableInfo?.cafeAddress)
                startActivity(intent)
            }

            val songsQueueButton = root.findViewById<Button>(R.id.songsQueueButton)
            songsQueueButton.setOnClickListener{
                val intent = Intent(activity, SongsQueueActivity::class.java)
                intent.putExtra("cafeId", tableInfo?.cafeId)
                startActivity(intent)
            }

            val orderDrinkButton = root.findViewById<Button>(R.id.orderDrinkButton)
            orderDrinkButton.setOnClickListener{
                val intent = Intent(activity, MenuActivity::class.java)
                intent.putExtra("cafeId", tableInfo?.cafeId)
                intent.putExtra("tableId", tableInfo?.tableId)
                startActivity(intent)
            }

            val orderSongButton = root.findViewById<Button>(R.id.orderSongButton)
            orderSongButton.setOnClickListener{
                val intent = Intent(activity, OrderSongActivity::class.java)
                intent.putExtra("cafeId", tableInfo?.cafeId)
                intent.putExtra("tableId", tableInfo?.tableId)
                startActivity(intent)
            }

            val suggestSongButton = root.findViewById<Button>(R.id.suggestSongButton)
            suggestSongButton.setOnClickListener {
                val intent = Intent(activity, SuggestSongActivity::class.java)
                intent.putExtra("tableId", tableInfo?.tableId)
                startActivity(intent)
            }


            if(!tableInfo?.quizSolvedBefore!!) {
                getNextQuiz(tableInfo?.cafeId, root, quizViewModel, resultsViewModel)
                eventButton.setOnClickListener{
                    val intent = Intent(activity, QuizQuestionActivity::class.java)
                    intent.putExtra("tableId", tableInfo?.tableId)
                    startActivity(intent)
                }
            } else {
                eventButton.isEnabled = true
                resultsAvailable = true
                resultsViewModel.getResultsAvailability()
                eventButton.setOnClickListener{
                    val intent = Intent(activity, QuizQuestionActivity::class.java)
                    intent.putExtra("tableId", tableInfo?.tableId)
                    getPlacements(eventId, intent)
                }
            }



            val leaveTableButton = root.findViewById<Button>(R.id.leaveTableButton)
            leaveTableButton.setOnClickListener{
                atTheTable.value = false
            }

            return root
        } catch (e: Exception) {
            println(TAG + "onCreateView" + e)
            throw e
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun timer(view: View, timeStamp: Timestamp, quizViewModel: QuizAvailabilityViewModel, resultsViewModel: ResultsAvailabilityViewModel) {
        GlobalScope.launch {
            withContext(Dispatchers.IO) {
                val nextEventTextView = view.findViewById<TextView>(R.id.nextEventTextView)
                val eventButton = view.findViewById<Button>(R.id.eventButton)
                var now: Timestamp = Timestamp(System.currentTimeMillis())
                var givenDuration = Duration.ofMillis(timeStamp.getTime())
                givenDuration = givenDuration.minusHours(1)
                var nowDuration = Duration.ofMillis(now.getTime())
                var duration = givenDuration.minus(nowDuration)
                if (duration.isNegative && duration.abs().seconds < 3600) {
                    quizAvailable = true
                    withContext(Dispatchers.Main) {
                        quizViewModel.getQuizAvailability()
                        eventButton.isEnabled = true
                        nextEventTextView.text = "Quiz ready"
                    }
                } else if(duration.isNegative) {
                    quizAvailable = false
                    withContext(Dispatchers.Main) {
                        quizViewModel.getQuizAvailability()
                        nextEventTextView.text = "You are late"
                        eventButton.isEnabled = false
                    }
                } else {
                    quizAvailable = false
                    withContext(Dispatchers.Main) {
                        eventButton.isEnabled = false
                        quizViewModel.getQuizAvailability()
                    }
                    while (true) {
                        delay(1000)
                        duration = duration.minus(1, ChronoUnit.SECONDS)
                        if(!duration.isZero && !duration.isNegative) {
                            var left = floor((duration.seconds / 3600).toDouble()).toInt().toString() + "h : "  + floor((duration.seconds % 3600).toDouble() / 60).toInt().toString() + "m : "  + ((duration.seconds % 3600).toDouble() % 60).toInt().toString() + "s"
                            withContext(Dispatchers.Main) {
                                nextEventTextView.text = left
                            }
                        } else {
                            quizAvailable = true
                            withContext(Dispatchers.Main) {
                                eventButton.isEnabled = true
                                quizViewModel.getQuizAvailability()
                                nextEventTextView.text = "Quiz ready"
                            }
                            break
                        }

                    }
                }
            }
        }
    }

    private fun getNextQuiz(cafeId: Long?, view: View, quizViewModel: QuizAvailabilityViewModel, resultsViewModel: ResultsAvailabilityViewModel) {

        if(cafeId == null) return

        RestFactory.instance(requireActivity().application).getNextQuiz(cafeId, object :
            Callback<EventResponse?> {

            @RequiresApi(Build.VERSION_CODES.O)
            override fun success(eventResponse: EventResponse?, response: Response?) {
                if(eventResponse == null) {
                    val nextEventTextView = view.findViewById<TextView>(R.id.nextEventTextView)
                    quizAvailable = false
                    this@CurrentTableFragment.quizViewModel.getQuizAvailability()
                    nextEventTextView.text = "No quiz in the next 24h!"
                    val eventButton = view.findViewById<Button>(R.id.eventButton)
                    eventButton.isEnabled = false
                } else {
                    eventId = eventResponse.id
                    var date = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX").parse(eventResponse.date)
                    timer(view, Timestamp(date.time), quizViewModel, resultsViewModel)
                }
            }

            override fun failure(error: RetrofitError?) {
                val nextEventTextView = view.findViewById<TextView>(R.id.nextEventTextView)
                quizAvailable = false
                this@CurrentTableFragment.quizViewModel.getQuizAvailability()
                nextEventTextView.text = "No quiz in the next 24h!"
                val eventButton = view.findViewById<Button>(R.id.eventButton)
                eventButton.isEnabled = false
            }

        } as Callback<EventResponse?>)
    }

    private fun getPlacements(eventId: Long?, intent: Intent) {

        if(eventId == null) return

        RestFactory.instance(requireActivity().application).getPlacements(eventId, object :
            Callback<List<PlacementsResponse>> {

            override fun success(placementsResponse: List<PlacementsResponse>, response: Response?) {
                initialisePlacements(placementsResponse)
                startActivity(intent)
            }

            override fun failure(error: RetrofitError?) {
                if(activity != null) {
                    val text = "Error with getting results placements"
                    Toast.makeText(activity?.application, text, Toast.LENGTH_SHORT).show()
                }
            }

        } as Callback<List<PlacementsResponse>>)
    }
}
