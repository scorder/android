package hr.fer.scorder.activities.songsQueueScreen

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import hr.fer.scorder.R
import hr.fer.scorder.entity.SongResponse
import kotlinx.android.synthetic.main.songs_item.view.*

class QueueAdapter(private val songList: List<SongResponse>) : RecyclerView.Adapter<QueueAdapter.SongsViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SongsViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.songs_item, parent, false)
        return SongsViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: SongsViewHolder, position: Int) {
        val currentItem = songList[position]
        holder.songName.text = if(!currentItem.name.equals("")) currentItem.name else "/"
        holder.songArtist.text = if(!currentItem.artist.equals("")) currentItem.artist else "/"
        holder.album.text = if(!currentItem.album.equals("")) currentItem.album else "/"

        if(position == 0) {
            holder.rbr.setTextColor(Color.GREEN)
            holder.rbr.textSize = 18F
            holder.rbr.text = "Playing: "
        } else {
            holder.rbr.text = position.toString() + ". "
        }

    }


    override fun getItemCount() = songList.size

    class SongsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val songName: TextView = itemView.songNameTextView
        val songArtist: TextView = itemView.songArtistTextView
        val album: TextView = itemView.albumTextView
        val rbr: TextView = itemView.rbrTV
    }

}
