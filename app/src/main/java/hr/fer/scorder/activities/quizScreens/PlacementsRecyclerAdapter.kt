package hr.fer.scorder.activities.menuScreen

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import hr.fer.scorder.activities.quizScreens.PlacementsViewHolder
import hr.fer.scorder.entity.PlacementsResponse

class PlacementsRecyclerAdapter (private var users: List<PlacementsResponse>) : RecyclerView.Adapter<PlacementsViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlacementsViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return PlacementsViewHolder(inflater, parent)

    }

    override fun getItemCount(): Int = users.size

    override fun onBindViewHolder(viewHolder: PlacementsViewHolder, position: Int) {
        val user: PlacementsResponse = users[position]
        viewHolder.bind(user)
    }

}
