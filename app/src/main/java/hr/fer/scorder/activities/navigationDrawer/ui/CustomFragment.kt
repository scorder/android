package hr.fer.scorder.activities.navigationDrawer.ui

import androidx.fragment.app.Fragment

abstract class CustomFragment: Fragment() {
    var label: String? = null
    var itemId: Int? = null
}
