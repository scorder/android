package hr.fer.scorder.activities.caffeScreen

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import hr.fer.scorder.R
import hr.fer.scorder.net.RestFactory
import kotlinx.android.synthetic.main.activity_events.*
import retrofit.Callback
import retrofit.RetrofitError
import retrofit.client.Response

class EventsActivity : AppCompatActivity() {

    lateinit var events: List<Event>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_events)

        val bundle = intent.extras
        var cafeId: Long? = null
        if (bundle != null) {
            cafeId = bundle.getLong("cafeId")
        } else {
            throw Exception("No cafeId passed to EventsActivity as extra!")
        }

        getEvents(cafeId)

    }

    fun getEvents(cafeId: Long?) {

        if(cafeId == null) return

        RestFactory.instance(applicationContext).getEvents(cafeId, object :
                Callback<List<Event>> {

            override fun success(eventsResponse: List<Event>, response: Response?) {
                events = eventsResponse

                events_recycler_view.adapter = EventsRecyclerAdapter(events)
                events_recycler_view.layoutManager = LinearLayoutManager(applicationContext)
                events_recycler_view.setHasFixedSize(true)
            }

            override fun failure(error: RetrofitError?) {
                val text = "Error with getting events"
                Toast.makeText(applicationContext, text, Toast.LENGTH_SHORT).show()
            }

        } as Callback<List<Event>>)
    }
}
