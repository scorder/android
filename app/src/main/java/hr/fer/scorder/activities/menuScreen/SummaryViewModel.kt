package hr.fer.scorder.activities.menuScreen

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import hr.fer.scorder.activities.menuScreen.BasketRepository

class SummaryViewModel: ViewModel() {
    var summary = MutableLiveData<String>()

    fun getSummary() {
        summary.value = BasketRepository.sum().toString()
    }
}
