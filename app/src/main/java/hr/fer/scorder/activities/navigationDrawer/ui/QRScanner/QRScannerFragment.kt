package hr.fer.scorder.activities.navigationDrawer.ui.QRScanner

import android.app.Activity
import android.content.pm.PackageManager
import android.location.Geocoder
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.budiyev.android.codescanner.AutoFocusMode
import com.budiyev.android.codescanner.CodeScanner
import com.budiyev.android.codescanner.CodeScannerView
import com.budiyev.android.codescanner.DecodeCallback
import com.budiyev.android.codescanner.ErrorCallback
import com.budiyev.android.codescanner.ScanMode
import hr.fer.scorder.R
import hr.fer.scorder.activities.navigationDrawer.ui.CustomFragment
import hr.fer.scorder.activities.navigationDrawer.ui.currentTable.TableInfo
import hr.fer.scorder.activities.navigationDrawer.ui.currentTable.currentTableInfo.atTheTable
import hr.fer.scorder.activities.navigationDrawer.ui.currentTable.currentTableInfo.tableInfo
import hr.fer.scorder.entity.ScanResponse
import hr.fer.scorder.net.RestFactory
import hr.fer.scorder.utils.ProgressBarController
import kotlinx.android.synthetic.main.fragment_qr_scanner.*
import retrofit.Callback
import retrofit.RetrofitError
import retrofit.client.Response
import java.util.*

const val CAMERA_REQUEST_CODE = 101

class QRScannerFragment : CustomFragment() {

    init {
        label = "Scan QR code"
        itemId = R.id.nav_QR_code
    }

    private lateinit var codeScanner: CodeScanner
    var waitingResponse = false
    var root: View? = null
    var progressBar: ProgressBar? = null
    companion object {
        var permissionGranted: Boolean = false
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        setupPermissions(requireActivity())
        root = inflater.inflate(R.layout.fragment_qr_scanner, container, false)
        progressBar = root!!.findViewById(R.id.progressBar)
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val scannerView = view.findViewById<CodeScannerView>(R.id.scanner_view)
        codeScanner(scannerView)
    }

    private fun codeScanner(scannerView: CodeScannerView) {
        val activity = requireActivity()
        codeScanner = CodeScanner(activity, scannerView)

        codeScanner.apply {
            camera = CodeScanner.CAMERA_BACK
            formats = CodeScanner.ALL_FORMATS

            autoFocusMode = AutoFocusMode.SAFE
            scanMode = ScanMode.PREVIEW
            isAutoFocusEnabled = true
            isFlashEnabled = false

            decodeCallback = DecodeCallback {

                codeScanner.scanMode = ScanMode.PREVIEW

                if(it.text.toLongOrNull() == null) {
                    activity.runOnUiThread {
                        Toast.makeText(requireActivity(), "Wrong QR code.", Toast.LENGTH_SHORT).show()
                    }
                    return@DecodeCallback
                }

                if(!waitingResponse) {
                    waitingResponse = true
                    activity.runOnUiThread {
                        ProgressBarController.enable(progressBar!!, requireActivity().window, root!!)
                    }
                    RestFactory.instance(requireContext()).sitAtTheTable(it.text.toLong(), object : Callback<ScanResponse> {

                        override fun success(scanResponse: ScanResponse?, response: Response?) {
                            ProgressBarController.disable(progressBar!!, requireActivity().window, root!!)
                            val geocoder = Geocoder(activity, Locale.getDefault())
                            val address = geocoder.getFromLocation(scanResponse!!.locationX!!, scanResponse.locationY!!, 1).get(0).getAddressLine(0)
                            tableInfo = TableInfo(scanResponse.tableId!!, scanResponse.tableNumber!!, scanResponse.cafeId!!,
                                    scanResponse.cafeName!!, address, scanResponse.locationX!!, scanResponse.locationY!!
                            )
                            atTheTable.value = true
                            waitingResponse = false
                        }

                        override fun failure(error: RetrofitError?) {
                            ProgressBarController.disable(progressBar!!, requireActivity().window, root!!)
                            if(error != null && error.response.status == 409)
                                Toast.makeText(activity, "Table is already occupied.", Toast.LENGTH_SHORT).show()
                            else
                                Toast.makeText(activity, "Bad QR code.", Toast.LENGTH_SHORT).show()
                            waitingResponse = false
                        }

                    } as Callback<ScanResponse>)
                }

            }

            errorCallback = ErrorCallback {
                activity.runOnUiThread {
                    Log.e("Main", "Camera initialization error: ${it.message}")
                }
            }

            scannerView.setOnClickListener {
                codeScanner.startPreview()
                codeScanner.scanMode = ScanMode.SINGLE
            }
        }
    }

    override fun onResume() {
        super.onResume()

        val permission: Int = ContextCompat.checkSelfPermission(requireContext(),
                android.Manifest.permission.CAMERA)
        if(permission == PackageManager.PERMISSION_GRANTED)
            codeScanner.startPreview()
    }

    override fun onPause() {
        codeScanner.releaseResources()
        super.onPause()
    }

    private fun setupPermissions(activity: Activity) {
        val permission: Int = ContextCompat.checkSelfPermission(activity,
                android.Manifest.permission.CAMERA)

        if(permission != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(arrayOf(android.Manifest.permission.CAMERA), CAMERA_REQUEST_CODE)
        } else {
            permissionGranted = true
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            CAMERA_REQUEST_CODE -> {
                if(grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(activity, "You need the camera permission to be able to use QR scanner.", Toast.LENGTH_LONG).show()
                    activity?.onBackPressed()
                } else {
                    codeScanner.startPreview()
                }
            }
        }
    }
}
