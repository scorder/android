package hr.fer.scorder.activities.quizScreens

import android.widget.Button
import hr.fer.scorder.entity.PlacementsResponse
import kotlinx.coroutines.*

object Quiz {
    var questions = mutableListOf<Question>()
    var on: Int = -1
    var answers = mutableMapOf<String, String>()
    var numCorrectA: Int? = null
    var numAllA: Int? = null
    var quizAvailable: Boolean = false
    var resultsAvailable: Boolean = false
    var eventId: Long? = null
    var usersPlacement: List<PlacementsResponse>? = null
    var counter: Int = 0

    fun cancel(numCorrect: Int?, numAll: Int?) {
        numCorrectA = numCorrect
        numAllA = numAll
        questions = mutableListOf<Question>()
        answers = mutableMapOf<String, String>()
        on = -1
        quizAvailable = false
    }

    fun timer(button: Button, onSent: Int) {
        GlobalScope.launch {
            withContext(Dispatchers.IO) {
                delay(20000)
                if(on - onSent == 0 || counter == questions.size) {
                    withContext(Dispatchers.Main) {
                        button.performClick()
                    }
                }
            }
        }
    }

    fun initialisePlacements(placementsResponse: List<PlacementsResponse>) {
        usersPlacement = placementsResponse
    }
}
