package hr.fer.scorder.activities.menuScreen

import com.google.gson.annotations.SerializedName

open class MenuItemModel (
    @SerializedName("id") var id: Long,
    @SerializedName("name") var name: String,
    @SerializedName("price" ) var price: Float,
    @SerializedName("servingSize") var servingSize: Float,
    @SerializedName("unitOfMeasure") var unitOfMeasure: String,
    @SerializedName("percentDiscount") var percentDiscount: Float
)
