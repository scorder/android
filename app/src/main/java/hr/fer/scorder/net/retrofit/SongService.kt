package hr.fer.scorder.net.retrofit

import hr.fer.scorder.entity.ExcludedSongsResponse
import hr.fer.scorder.entity.OrderSongRequest
import hr.fer.scorder.entity.SongResponse
import hr.fer.scorder.entity.SuggestSongRequest
import retrofit.Callback
import retrofit.http.Body
import retrofit.http.GET
import retrofit.http.POST
import retrofit.http.Query

interface SongService {

    @GET("/cafe/songs")
    fun getSongs(@Query("id") cafeId: Long, songsResponse: Callback<List<SongResponse>>)

    @POST("/cafe/songs/order")
    fun orderSong(@Body orderSongRequest: OrderSongRequest, orderResponse: Callback<Void?>)

    @POST("/cafe/songs/suggest")
    fun suggestSong(@Body suggestSongRequest: SuggestSongRequest, response: Callback<Void?>)

    @GET("/cafe/songs/excludedSongs")
    fun getExcludedSongs(@Query("cafeId") cafeId: Long, excludedSongsResponse: Callback<ExcludedSongsResponse>)

    @GET("/cafe/songs/songsQueue")
    fun getSongsQueue(@Query("cafeId") cafeId: Long, songsQueueResponse: Callback<List<SongResponse>>)
}
