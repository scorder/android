package hr.fer.scorder.net.retrofit

import hr.fer.scorder.net.RestInterface
import android.content.Context
import hr.fer.scorder.activities.caffeScreen.Event
import hr.fer.scorder.activities.menuScreen.MenuItemModel
import hr.fer.scorder.activities.quizScreens.Question
import hr.fer.scorder.entity.*
import hr.fer.scorder.utils.SharedPrefManager
import retrofit.Callback
import retrofit.RequestInterceptor
import retrofit.RestAdapter
import java.sql.Timestamp


class RestRetrofit(ctx: Context) : RestInterface {
    private val userService: UserService
    private val caffeService: CaffeService
    private val tableService: TableService
    private val songService: SongService
    private var AUTH: String? = null

    init {
        val baseURL = "http://hr-fer-scorder.herokuapp.com/api/"

        AUTH = SharedPrefManager.getInstance(ctx).loginResponse.tokenType + " " + SharedPrefManager.getInstance(ctx).loginResponse.accessToken

        val requestInterceptor = RequestInterceptor { request ->
            request.addHeader("Authorization", AUTH)
        }

        val retrofit = RestAdapter.Builder()
                .setEndpoint(baseURL)
                .setRequestInterceptor(requestInterceptor)
                .build()

        userService = retrofit.create(UserService::class.java)
        caffeService = retrofit.create(CaffeService::class.java)
        tableService = retrofit.create(TableService::class.java)
        songService = retrofit.create(SongService::class.java)
    }

    override fun registerUser(newUser: RegisterUserRequest, registerResponse: Callback<Void?>) {
        userService.registerUser(newUser, registerResponse)
    }

    override fun loginUser(loginRequest: LoginRequest, loginResponse: Callback<LoginResponse>) {
        userService.loginUser(loginRequest, loginResponse)
    }

    override fun getCurrentUser(currentUserResponse: Callback<UserResponse>) {
        userService.getCurrentUser(currentUserResponse)
    }

    override fun editCurrentUser(editedUser: EditUserRequest, editCurrentUserResponse: Callback<UserResponse>) {
        userService.editCurrentUser(editedUser, editCurrentUserResponse)
    }

    override fun nearbyCaffes(nearbyCaffesRequest: NearbyCaffesRequest, nearbyCaffesResponse: Callback<List<CaffeResponse>>) {
        caffeService.nearbyCaffes(nearbyCaffesRequest, nearbyCaffesResponse)
    }

    override fun sitAtTheTable(tableId: Long, response: Callback<ScanResponse>) {
        tableService.sitAtTheTable(tableId, response)
    }

    override fun getSongs(cafeId: Long, songsResponse: Callback<List<SongResponse>>) {
        songService.getSongs(cafeId, songsResponse)
    }

    override fun orderSong(orderSongRequest: OrderSongRequest, orderResponse: Callback<Void?>) {
        songService.orderSong(orderSongRequest, orderResponse)
    }

    override fun suggestSong(suggestSongRequest: SuggestSongRequest, response: Callback<Void?>) {
        songService.suggestSong(suggestSongRequest, response)
    }

    override fun leave(tableId: Long, response: Callback<Void?>) {
        tableService.leave(tableId, response)
    }

    override fun getQuestions(eventId: Long, questionsResponse: Callback<List<Question>>) {
        caffeService.getQuestions(eventId, questionsResponse)
    }

    override fun sendAnswers(placeAnswers: PlaceAnswers, resultsResponse: Callback<List<Int>>) {
        caffeService.sendAnswers(placeAnswers, resultsResponse)
    }

    override fun getExcludedSongs(cafeId: Long, excludedSongsResponse: Callback<ExcludedSongsResponse>) {
        songService.getExcludedSongs(cafeId, excludedSongsResponse)
    }

    override fun getSongsQueue(cafeId: Long, songsQueueResponse: Callback<List<SongResponse>>) {
        songService.getSongsQueue(cafeId, songsQueueResponse)
    }

    override fun getNextQuiz(cafeId: Long, eventResponse: Callback<EventResponse?>) {
        caffeService.getNextQuiz(cafeId, eventResponse)
    }

    override fun getPlacements(eventId: Long, placementsResponse: Callback<List<PlacementsResponse>>) {
        caffeService.getPlacements(eventId, placementsResponse)
    }

    override fun getMenu(cafeId: Long, menuResponse: Callback<List<MenuItemModel>>) {
        caffeService.getMenu(cafeId, menuResponse)
    }

    override fun placeOrder(placeOrderRequest: PlaceOrderRequest, placeOrderResponse: Callback<Void?>) {
        caffeService.placeOrder(placeOrderRequest, placeOrderResponse)
    }

    override fun getEvents(cafeId: Long, eventsResponse: Callback<List<Event>>) {
        caffeService.getEvents(cafeId, eventsResponse)
    }
}