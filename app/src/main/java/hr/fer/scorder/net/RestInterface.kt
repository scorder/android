package hr.fer.scorder.net

import hr.fer.scorder.activities.caffeScreen.Event
import hr.fer.scorder.activities.menuScreen.MenuItemModel
import hr.fer.scorder.activities.quizScreens.Question
import hr.fer.scorder.entity.*
import retrofit.Callback
import java.sql.Timestamp

interface RestInterface {

    fun registerUser(newUser: RegisterUserRequest, registerResponse: Callback<Void?>)
    fun loginUser(loginRequest: LoginRequest, loginResponse: Callback<LoginResponse>)
    fun getCurrentUser(currentUserResponse: Callback<UserResponse>)
    fun editCurrentUser(editedUser: EditUserRequest, editCurrentUserResponse: Callback<UserResponse>)
    fun nearbyCaffes(nearbyCaffesRequest: NearbyCaffesRequest, nearbyCaffesResponse: Callback<List<CaffeResponse>>)
    fun sitAtTheTable(tableId: Long, response: Callback<ScanResponse>)
    fun getMenu(cafeId: Long, menuResponse: Callback<List<MenuItemModel>>)
    fun placeOrder(placeOrderRequest: PlaceOrderRequest, placeOrderResponse: Callback<Void?>)
    fun getEvents(cafeId: Long, eventsResponse: Callback<List<Event>>)
    fun getSongs(cafeId: Long, songsResponse: Callback<List<SongResponse>>)
    fun orderSong(orderSongRequest: OrderSongRequest, orderResponse: Callback<Void?>)
    fun suggestSong(suggestSongRequest: SuggestSongRequest, response: Callback<Void?>)
    fun leave(tableId: Long, response: Callback<Void?>)
    fun getQuestions(eventId: Long, questionsResponse: Callback<List<Question>>)
    fun sendAnswers(placeAnswers: PlaceAnswers, resultsResponse: Callback<List<Int>>)
    fun getExcludedSongs(cafeId: Long, excludedSongsResponse: Callback<ExcludedSongsResponse>)
    fun getSongsQueue(cafeId: Long, songsQueueResponse: Callback<List<SongResponse>>)
    fun getNextQuiz(cafeId: Long, eventResponse: Callback<EventResponse?>)
    fun getPlacements(eventId: Long, placementsResponse: Callback<List<PlacementsResponse>>)
}