package hr.fer.scorder.net.retrofit

import hr.fer.scorder.entity.*
import retrofit.Callback
import retrofit.http.*

interface UserService {

    @POST("/user/register")
    fun registerUser(@Body newUser: RegisterUserRequest, registerResponse: Callback<Void?>)

    @POST("/user/login")
    fun loginUser(@Body loginRequest: LoginRequest, loginResponse: Callback<LoginResponse>)

    @GET("/user/current")
    fun getCurrentUser(currentUserResponse: Callback<UserResponse>)

    @PATCH("/user/update")
    fun editCurrentUser(@Body editedUser: EditUserRequest, editCurrentUserResponse: Callback<UserResponse>)
}
