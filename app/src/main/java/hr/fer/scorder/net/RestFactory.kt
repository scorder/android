package hr.fer.scorder.net

import hr.fer.scorder.net.retrofit.RestRetrofit
import android.content.Context

object RestFactory {

    fun instance(ctx: Context): RestInterface {
        return RestRetrofit(ctx)
    }
}
