package hr.fer.scorder.net.retrofit

import hr.fer.scorder.activities.menuScreen.MenuItemModel
import hr.fer.scorder.activities.caffeScreen.Event
import hr.fer.scorder.activities.quizScreens.Question
import hr.fer.scorder.entity.*
import retrofit.Callback
import retrofit.http.*

interface CaffeService {

    @POST("/cafe/nearby")
    fun nearbyCaffes(@Body nearbyCaffesRequest: NearbyCaffesRequest, nearbyCaffesResponse: Callback<List<CaffeResponse>>)

    @GET("/cafe/item/menu")
    fun getMenu(@Query("cafeId") cafeId: Long?, menuResponse: Callback<List<MenuItemModel>>)

    @POST("/order/add")
    fun placeOrder(@Body placeOrderRequest: PlaceOrderRequest, placeOrderResponse: Callback<Void?>)

    @GET("/cafe/events/{cafeId}")
    fun getEvents(@Path("cafeId") cafeId: Long?, eventsResponse: Callback<List<Event>>)

    @GET("/cafe/events/quiz/{eventId}")
    fun getQuestions(@Path("eventId") eventId: Long, questionsResponse: Callback<List<Question>>)

    @POST("/cafe/events/quiz/answers")
    fun sendAnswers(@Body placeAnswers: PlaceAnswers, resultsResponse: Callback<List<Int>>)

    @GET("/cafe/events/next{cafeId}")
    fun getNextQuiz(@Path("cafeId") cafeId: Long, eventResponse: Callback<EventResponse?>)

    @GET("/cafe/events/placements/{eventId}")
    fun getPlacements(@Path("eventId") eventId: Long, placementsResponse: Callback<List<PlacementsResponse>>)
}
