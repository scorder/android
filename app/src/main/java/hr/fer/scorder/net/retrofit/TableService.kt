package hr.fer.scorder.net.retrofit

import hr.fer.scorder.entity.ScanResponse
import retrofit.Callback
import retrofit.http.GET
import retrofit.http.Query

interface TableService {

    @GET("/cafe/tables/scan")
    fun sitAtTheTable(@Query("tableId") tableId: Long, response: Callback<ScanResponse>)

    @GET("/cafe/tables/leave")
    fun leave(@Query("tableId") tableId: Long, response: Callback<Void?>)
}
