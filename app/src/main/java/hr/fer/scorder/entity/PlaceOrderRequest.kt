package hr.fer.scorder.entity

import com.google.gson.annotations.SerializedName

class PlaceOrderRequest (
    @SerializedName("tableId") var tableId: String?,
    @SerializedName("orderEntries") var orderList: List<OrderEntry>?
)