package hr.fer.scorder.entity

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class SongResponse (
    @SerializedName("id") var id: Long?,
    @SerializedName("songTitle") var name: String?,
    @SerializedName("artistName") var artist: String?,
    @SerializedName("albumTitle") var album: String?,
    @SerializedName("songLink") var link: String?
): Serializable
