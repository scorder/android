package hr.fer.scorder.entity

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class UserResponse (
        @SerializedName("id") var id: String?,
        @SerializedName("firstName") var firstName: String?,
        @SerializedName("lastName") var lastName: String?,
        @SerializedName("email") var email: String?,
        @SerializedName("username") var username: String?
): Serializable
