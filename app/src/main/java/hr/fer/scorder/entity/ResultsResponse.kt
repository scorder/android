package hr.fer.scorder.entity

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class ResultsResponse (
    @SerializedName("numCorrectAnswers") var numCorrectAnswers: Int?,
    @SerializedName("numAllAnswers") var numAllAnswers: Int?
): Serializable
