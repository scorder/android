package hr.fer.scorder.entity

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class ExcludedSongsResponse(
        @SerializedName("excludedPreviousSongIds") var prev: List<Long>?,
        @SerializedName("excludedFutureSongIds") var fut: List<Long>?
): Serializable
