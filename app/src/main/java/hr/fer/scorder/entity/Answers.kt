package hr.fer.scorder.entity

import com.google.gson.annotations.SerializedName
import kotlinx.serialization.Serializable

data class Answers (
    @SerializedName("question") var question: String?,
    @SerializedName("answer") var answer: String?
)
