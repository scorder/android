package hr.fer.scorder.entity

import com.google.gson.annotations.SerializedName
import kotlinx.serialization.Serializable

data class OrderEntry (
    @SerializedName("itemId") var id: Long?,
    @SerializedName("quantity") var count: Int?
)
