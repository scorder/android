package hr.fer.scorder.entity

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class EditUserRequest(
        @SerializedName("firstName") var firstName: String?,
        @SerializedName("lastName") var lastName: String?,
        @SerializedName("email") var email: String?,
        @SerializedName("username") var username: String?,
        @SerializedName("password") var password: String?
): Serializable
