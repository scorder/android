package hr.fer.scorder.entity

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class OrderSongRequest(
        @SerializedName("tableId") var tableId: Long? = null,
        @SerializedName("songIds") var songIds: LongArray? = null
): Serializable
