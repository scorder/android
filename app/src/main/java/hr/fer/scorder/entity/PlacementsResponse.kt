package hr.fer.scorder.entity

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class PlacementsResponse (
    @SerializedName("place") var place: Int?,
    @SerializedName("username") var username: String?,
    @SerializedName("percentage") var percentage: Long?
): Serializable
