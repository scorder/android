package hr.fer.scorder.entity

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class CaffeResponse(
    @SerializedName("id") var id: Long? = null,
    @SerializedName("name") var name: String? = null,
    @SerializedName("email") var email: String? = null,
    @SerializedName("locationX") var locationX: Double? = null,
    @SerializedName("locationY") var locationY: Double? = null,
    @SerializedName("takenTables") var takenTables: Int? = null,
    @SerializedName("totalTables") var totalTables: Int? = null
): Serializable
