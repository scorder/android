package hr.fer.scorder.entity

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class LoginRequest (
        @SerializedName("email") var email: String?,
        @SerializedName("password") var password: String?
): Serializable
