package hr.fer.scorder.entity

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class LoginResponse (
    @SerializedName("accessToken") var accessToken: String?,
    @SerializedName("email") var email: String?,
    @SerializedName("tokenType") var tokenType: String?
): Serializable
