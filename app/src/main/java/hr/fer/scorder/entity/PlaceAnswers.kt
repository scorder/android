package hr.fer.scorder.entity

import com.google.gson.annotations.SerializedName

class PlaceAnswers (
    @SerializedName("tableId") var tableId: String?,
    @SerializedName("eventId") var eventId: String?,
    @SerializedName("answers") var answers: List<Answers>?
)
