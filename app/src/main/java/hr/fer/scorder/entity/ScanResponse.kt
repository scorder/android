package hr.fer.scorder.entity

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class ScanResponse (
        @SerializedName("tableId") var tableId: Long?,
        @SerializedName("tableNumber") var tableNumber: Int?,
        @SerializedName("cafeId") var cafeId: Long?,
        @SerializedName("cafeName") var cafeName: String?,
        @SerializedName("locationX") var locationX: Double?,
        @SerializedName("locationY") var locationY: Double?
): Serializable
