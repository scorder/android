package hr.fer.scorder.entity

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class NearbyCaffesRequest(
    @SerializedName("latitude") var latitude: Double? = null,
    @SerializedName("longitude") var longitude: Double? = null,
    @SerializedName("latitudeDeviation") var latitudeDeviation: Double? = null,
    @SerializedName("longitudeDeviation") var longitudeDeviation: Double? = null
): Serializable
