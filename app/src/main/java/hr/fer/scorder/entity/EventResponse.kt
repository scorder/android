package hr.fer.scorder.entity

import com.google.gson.annotations.SerializedName
import java.io.Serializable
import java.sql.Timestamp
import java.time.LocalDateTime

class EventResponse (
    @SerializedName("id") var id: Long?,
    @SerializedName("eventName") var eventName: String?,
    @SerializedName("date") var date: String?,
    @SerializedName("ending") var ending: String?,
    @SerializedName("eventInfo") var eventInfo: String?
): Serializable
